package com.blueprint.cansearch.main

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.animation.AccelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.blueprint.cansearch.R
import com.blueprint.cansearch.search.ui.SearchClinicalTrialsViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var bottomNav: BottomNavigationView
    private var bottomNavEnabled = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNav = findViewById(R.id.bottomNav)
        setNavClickListener()
        setupBottomNavListener()
    }

    private fun setNavClickListener() {
        bottomNav.setupWithNavController(Navigation.findNavController(this, R.id.searchFragment))
    }

    private fun setupBottomNavListener() {
        val nav = Navigation.findNavController(this, R.id.searchFragment)
        nav.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.searchFragment,
                R.id.archivedFragment,
                R.id.settingsFragment -> {
                    if (!bottomNavEnabled) {
                        animateUpBottomNav()
                        bottomNavEnabled = true
                    }
                }
                R.id.detailedClinicalTrialFragment,
                R.id.locationsFragment,
                R.id.sitesFragment -> {
                    animateDownBottomNav()
                    bottomNavEnabled = false
                }
            }
        }
    }

    private fun animateDownBottomNav() {
        val dp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 56f, bottomNav.context.resources.displayMetrics)

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(
            ObjectAnimator.ofFloat(bottomNav, View.ALPHA, 0f),
            ObjectAnimator.ofFloat(bottomNav, View.TRANSLATION_Y, dp)
        )
        animatorSet.duration = 100
        animatorSet.interpolator = AccelerateInterpolator()
        animatorSet.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                bottomNav.visibility = View.GONE
            }
        })
        animatorSet.start()
    }

    private fun animateUpBottomNav() {
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(
            ObjectAnimator.ofFloat(bottomNav, View.ALPHA, 1f),
            ObjectAnimator.ofFloat(bottomNav, View.TRANSLATION_Y, 0f)
        )
        animatorSet.duration = 300
        animatorSet.interpolator = FastOutSlowInInterpolator()
        animatorSet.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator?) {
                super.onAnimationStart(animation)
                bottomNav.visibility = View.VISIBLE
            }
        })
        animatorSet.start()
    }

}