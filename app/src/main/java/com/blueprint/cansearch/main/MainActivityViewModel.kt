package com.blueprint.cansearch.main

import com.airbnb.mvrx.*
import com.blueprint.cansearch.BuildConfig
import com.blueprint.cansearch.search.domain.ClinicalTrials

data class MainActivitySearchState(val clinicalTrialsSearchResult: Async<Pair<ClinicalTrials?, String?>> = Uninitialized) : MvRxState

class MainActivityViewModel(
    val initialState: MainActivitySearchState = MainActivitySearchState()
) : BaseMvRxViewModel<MainActivitySearchState>
    (initialState = initialState, debugMode = BuildConfig.DEBUG) {

    fun clear(){
        setState {
            copy(clinicalTrialsSearchResult = Success(Pair(null, null)))
        }
    }

    fun set(results: Pair<ClinicalTrials, String>) {
        setState {
            copy(clinicalTrialsSearchResult = Success(results))
        }
    }

    companion object : MvRxViewModelFactory<MainActivityViewModel, MainActivitySearchState> {
        @JvmStatic
        override fun create(viewModelContext: ViewModelContext, state: MainActivitySearchState): MainActivityViewModel {
            return MainActivityViewModel(state)
        }
    }
}