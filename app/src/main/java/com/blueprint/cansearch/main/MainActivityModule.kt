package com.blueprint.cansearch.main

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    single { MainActivitySearchState() }
    viewModel {
        MainActivityViewModel(initialState = get())
    }
}