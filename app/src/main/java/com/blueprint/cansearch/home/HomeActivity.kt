package com.blueprint.cansearch.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.blueprint.cansearch.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}