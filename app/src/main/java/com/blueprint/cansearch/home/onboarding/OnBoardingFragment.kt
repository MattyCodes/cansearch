package com.blueprint.cansearch.home.onboarding

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.blueprint.cansearch.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator

class OnBoardingFragment : Fragment() {

    private lateinit var imageOnBoarding: ImageView
    private lateinit var viewpagerOnBoarding: ViewPager
    private lateinit var fabOnBoardingNext: FloatingActionButton
    private lateinit var tabIndicator: WormDotsIndicator

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_onboarding, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageOnBoarding = view.findViewById(R.id.imageOnBoarding)
        Glide.with(this).load(R.drawable.onboarding).centerCrop().into(imageOnBoarding)

        viewpagerOnBoarding = view.findViewById(R.id.viewpagerOnBoarding)
        tabIndicator = view.findViewById(R.id.tabIndicator)
        viewpagerOnBoarding.adapter = OnBoardingAdapter()
        tabIndicator.setViewPager(viewpagerOnBoarding)

        fabOnBoardingNext = view.findViewById(R.id.fabOnBoardingNext)

        animateFabIn()
        setFabOnClickListener()
    }

    override fun onResume() {
        super.onResume()
        viewpagerOnBoarding.setCurrentItem(0, false)
    }

    private fun animateFabIn() {

        val scaleX = ObjectAnimator.ofFloat(fabOnBoardingNext, View.SCALE_X, 0f, 1f)
        val scaleY = ObjectAnimator.ofFloat(fabOnBoardingNext, View.SCALE_Y, 0f, 1f)
        val alpha = ObjectAnimator.ofFloat(fabOnBoardingNext, View.ALPHA, 0f, 1f)

        AnimatorSet().apply {
            playTogether(scaleX, scaleY, alpha)
            duration = 650
            interpolator = OvershootInterpolator(1.5f)
            startDelay = 100
        }.start()
    }

    private fun setFabOnClickListener() {
        fabOnBoardingNext.setOnClickListener {
            when (viewpagerOnBoarding.currentItem) {
                0 -> {
                    viewpagerOnBoarding.setCurrentItem(1, true)
                }
                1 -> {
                    viewpagerOnBoarding.setCurrentItem(2, true)
                }
                2 -> {
                    it.findNavController().navigate(R.id.action_onBoardingFragment2_to_mainActivity2)
                }
            }
        }
    }
}