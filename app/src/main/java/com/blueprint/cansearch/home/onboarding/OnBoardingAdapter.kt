package com.blueprint.cansearch.home.onboarding

import android.view.View
import androidx.viewpager.widget.PagerAdapter
import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.TextView
import com.blueprint.cansearch.R

class OnBoardingAdapter : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        container.removeView(view as View)
    }

    override fun getCount() = ON_BOARDING_COUNT

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(container.context)
        val view = inflater.inflate(R.layout.item_onboarding_description, container, false) as ViewGroup
        val textTitle = view.findViewById<TextView>(R.id.textOnBoardingTitle)
        val textDescription = view.findViewById<TextView>(R.id.textOnBoardingDescription)

        when (position) {
            0 -> {
                textTitle.text = container.context.getText(R.string.onboarding_search_title)
                textDescription.text = container.context.getText(R.string.onboarding_search_description)
            }
            1 -> {
                textTitle.text = container.context.getText(R.string.onboarding_save_title)
                textDescription.text = container.context.getText(R.string.onboarding_save_description)
            }
            2 -> {
                textTitle.text = container.context.getText(R.string.onboarding_contact_title)
                textDescription.text = container.context.getText(R.string.onboarding_contact_description)
            }
        }
        container.addView(view)
        return view
    }

    companion object {
        const val ON_BOARDING_COUNT = 3
    }

}