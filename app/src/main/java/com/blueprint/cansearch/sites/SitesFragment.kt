package com.blueprint.cansearch.sites

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.blueprint.cansearch.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.core.app.ActivityCompat
import com.blueprint.cansearch.locations.domain.LocationDetail

const val locationsKey = "location"
const val phoneContactPrefix = "Call on:"
const val telPrefix = "tel:"

class SitesFragment : Fragment(), OnMapReadyCallback {

    private lateinit var locationDetail: LocationDetail

    private lateinit var map: MapView
    private lateinit var siteSummaryCompoundView: SiteSummaryCompoundView
    private lateinit var textPhoneNumber: TextView
    private lateinit var textEmail: TextView
    private lateinit var divider: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationDetail = arguments?.get(locationsKey) as LocationDetail
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_site_location, container, false)
        map = root.findViewById(R.id.map)
        textPhoneNumber = root.findViewById(R.id.textPhoneNumber)
        textEmail = root.findViewById(R.id.textEmail)
        divider = root.findViewById(R.id.divider)
        map.onCreate(savedInstanceState)
        siteSummaryCompoundView = root.findViewById(R.id.siteSummaryCompoundView)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        siteSummaryCompoundView.setData(locationDetail)
        setPhoneContact()
        setEmailContact()
    }

    private fun setEmailContact() {
        locationDetail.contactEmail?.let {
            textEmail.setOnClickListener {
            }
        } ?: run {
            textEmail.visibility = View.GONE
        }
    }

    private fun setPhoneContact() {
        locationDetail.orgPhoneme?.let {
            textPhoneNumber.text = "$phoneContactPrefix $it"
            textPhoneNumber.setOnClickListener { _->
                val intent = Intent(Intent.ACTION_CALL)
                intent.data = Uri.parse("$telPrefix$it")
                if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), 200)
                } else {
                    requireContext().startActivity(intent)
                }
            }
        } ?: run {
            textPhoneNumber.visibility = View.GONE
            divider.visibility = View.GONE
        }
    }

    override fun onStop() {
        super.onStop()
        map.onStop()
    }

    override fun onResume() {
        super.onResume()
        map.getMapAsync(this)
        map.onResume()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        map.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }


    override fun onMapReady(map: GoogleMap) {
        locationDetail.latLong?.let {
            map.mapType = GoogleMap.MAP_TYPE_TERRAIN
            val uman = LatLng(it.latitude, it.longitude)
            map.addMarker(MarkerOptions().position(uman).title(locationDetail.orgName?.let { it }))
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(uman, 15f))
        }
    }
}