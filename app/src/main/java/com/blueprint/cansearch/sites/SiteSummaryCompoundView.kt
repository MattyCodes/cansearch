package com.blueprint.cansearch.sites

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.blueprint.cansearch.R
import com.blueprint.cansearch.locations.domain.LocationDetail

const val recruitmentStatusPrefix = "Recruitment status:"

class SiteSummaryCompoundView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    ConstraintLayout(context, attrs, defStyleAttr) {

    private var textSiteSummaryTitle: TextView
    private var textRecruitmentStatus: TextView
    private var textAddressLine: TextView
    private var textAddressCityState: TextView
    private var textAddressZip: TextView

    init {
        inflate(context, R.layout.compound_view_site_summary, this)
        textSiteSummaryTitle = rootView.findViewById(R.id.textSiteSummaryTitle)
        textRecruitmentStatus = rootView.findViewById(R.id.textRecruitmentStatus)
        textAddressLine = rootView.findViewById(R.id.textAddressLine)
        textAddressCityState = rootView.findViewById(R.id.textAddressCityState)
        textAddressZip = rootView.findViewById(R.id.textAddressZip)
    }

    fun setData(locationDetail: LocationDetail){
        setTitle(locationDetail.orgName)
        setRecruitmentState(locationDetail.recruitmentStatus)
        setAddressLine(locationDetail.addressLine)
        setCityState(locationDetail.orgCity, locationDetail.orgStateOrProvince)
        setZipCode(locationDetail.orgPostalCode)
    }

    private fun setZipCode(orgPostalCode: String?) {
        orgPostalCode?.let {
            textAddressZip.text = it
        } ?: run {
            textAddressZip.visibility = View.GONE
        }
    }

    private fun setCityState(orgCity: String?, orgStateOrProvince: String?) {
        if (orgCity != null && orgStateOrProvince != null){
            textAddressCityState.text = "$orgCity, $orgStateOrProvince"
        } else {
            textAddressCityState.visibility = View.GONE
        }
    }

    private fun setAddressLine(addressLine: String?) {
        addressLine?.let {
            textAddressLine.text = it
        } ?: run {
            textAddressLine.visibility = View.GONE
        }
    }

    private fun setRecruitmentState(recruitmentStatus: String?) {
        recruitmentStatus?.let {
            textRecruitmentStatus.text = "$recruitmentStatusPrefix $it"
        } ?: run {
            textRecruitmentStatus.visibility = View.GONE
        }
    }

    private fun setTitle(orgName: String?) {
        orgName?.let {
            textSiteSummaryTitle.text = it
        } ?: run {
            textSiteSummaryTitle.text = resources.getString(R.string.site_summary_title)
        }
    }

}