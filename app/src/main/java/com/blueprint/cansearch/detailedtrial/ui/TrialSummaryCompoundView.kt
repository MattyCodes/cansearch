package com.blueprint.cansearch.detailedtrial.ui

import android.content.Context
import android.util.AttributeSet
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.blueprint.cansearch.R
import com.blueprint.cansearch.detailedtrial.domain.StudySummaryItem

class TrialSummaryCompoundView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var containerTrialSummary: LinearLayout
    private var textLocationsNumber: TextView
    private var buttonTrialLocations: Button

    init {
        inflate(context, R.layout.trials_summary_compound, this)
        containerTrialSummary = rootView.findViewById(R.id.containerTrialSummary)
        textLocationsNumber = rootView.findViewById(R.id.textLocationsNumber)
        buttonTrialLocations = rootView.findViewById(R.id.buttonTrialLocations)
    }

    fun setData(trialSummaryList: List<StudySummaryItem>, numberOfLocations: Int) {
        textLocationsNumber.text = "$numberOfLocations"
        containerTrialSummary.removeAllViews()
        trialSummaryList.forEach {
            val studySummaryView = TrialSummaryItemCompoundView(context)
            studySummaryView.setSummaryItem(it)
            containerTrialSummary.addView(studySummaryView)
        }
    }

}