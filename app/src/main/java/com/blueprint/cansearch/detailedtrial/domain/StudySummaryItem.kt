package com.blueprint.cansearch.detailedtrial.domain

data class StudySummaryItem(val title: String, val value: String, val color: Int)