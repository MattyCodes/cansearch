package com.blueprint.cansearch.detailedtrial.ui

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import com.airbnb.mvrx.*
import com.bumptech.glide.Glide
import com.blueprint.cansearch.R
import com.blueprint.cansearch.detailedtrial.*
import com.blueprint.cansearch.search.domain.ClinicalTrials
import com.blueprint.cansearch.search.domain.mapToEligibilityCriteria
import com.blueprint.cansearch.search.domain.mapToStudySummary
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import android.text.style.UnderlineSpan
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import com.blueprint.cansearch.common.domain.ArchivedArticle
import com.blueprint.cansearch.search.domain.Associated
import com.blueprint.cansearch.search.ui.SearchClinicalTrialsViewModel
import org.joda.time.DateTime

class DetailedClinicalTrialFragment : BaseMvRxFragment() {

    private val detailedClinicalTrialViewModel: DetailedClinicalTrialViewModel by fragmentViewModel()
    private val searchClinicalTrialsViewModel: SearchClinicalTrialsViewModel by activityViewModel()

    // id
    private var trialId: String? = null
    private var storedArticleId: String? = null
    private var searchQuery: String? = null

    // view
    private lateinit var ivBack: ImageView
    private lateinit var clLoadingSplash: ConstraintLayout
    private lateinit var textBriefTitle: TextView
    private lateinit var compoundTrialSummary: StudySummaryCompoundView
    private lateinit var compoundTrial: TrialSummaryCompoundView
    private lateinit var compoundTrialEligibility: EligibilityCompoundView
    private lateinit var compoundAssociatedDiseases: AssociatedDiseaseInfoCompoundView
    private lateinit var buttonScientificSummaryDetail: Button
    private lateinit var buttonTrialLocations: Button
    private lateinit var textScientificDescription: TextView
    private lateinit var bottom_sheet: FrameLayout
    private lateinit var imageScientificDetailBackground: ImageView
    private lateinit var fabStoreTrial: ExtendedFloatingActionButton
    private lateinit var bottomSheetBehaviour: BottomSheetBehavior<FrameLayout>

    private lateinit var onBackPressedCallback: OnBackPressedCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trialId = arguments?.get("trialId") as String?
        storedArticleId = arguments?.get("storedArticleId") as String?
        searchQuery = arguments?.get("searchQuery") as String?
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_detailed_clinical_trial, container, false)
        clLoadingSplash = root.findViewById(R.id.clLoadingSplash)
        textBriefTitle = root.findViewById(R.id.textBriefTitle)
        ivBack = root.findViewById(R.id.ivBack)
        compoundTrialSummary = root.findViewById(R.id.compoundTrialSummary)
        compoundTrial = root.findViewById(R.id.compoundTrial)
        compoundTrialEligibility = root.findViewById(R.id.compoundTrialEligibility)
        compoundAssociatedDiseases = root.findViewById(R.id.compoundAssociatedDiseases)
        buttonScientificSummaryDetail = compoundTrialSummary.findViewById(R.id.buttonScientificSummaryDetail)
        textScientificDescription = root.findViewById(R.id.textScientificDescription)
        bottom_sheet = root.findViewById(R.id.bottom_sheet)
        bottomSheetBehaviour = BottomSheetBehavior.from(bottom_sheet)
        imageScientificDetailBackground = root.findViewById(R.id.imageScientificDetailBackground)
        fabStoreTrial = root.findViewById(R.id.fabStoreTrial)
        buttonTrialLocations = compoundTrial.findViewById(R.id.buttonTrialLocations)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(imageScientificDetailBackground)
            .load(R.drawable.scientific_explanation_header)
            .centerCrop()
            .into(imageScientificDetailBackground)
        onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (bottomSheetBehaviour.state == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehaviour.state = BottomSheetBehavior.STATE_COLLAPSED
                }
            }
        }
        bottomSheetBehaviour.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback(){
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when(newState){
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        onBackPressedCallback.isEnabled = false
                        onBackPressedCallback.let {
                            requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, it)
                        }
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        onBackPressedCallback.isEnabled = true
                        onBackPressedCallback.let {
                            requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, it)
                        }
                    }
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        subscribeToClinicalTrial()
        subscribeToIsStored()

        storedArticleId?.let {
            detailedClinicalTrialViewModel.retrieveTrial(storedArticleId!!)
        } ?: run {
            withState(searchClinicalTrialsViewModel) { state ->
                val trial = state.clinicalTrialsSearchResult.invoke()!!.first.trials.find { it.nciID == trialId || it.nctID == trialId }
                trial?.let {
                    detailedClinicalTrialViewModel.setTrial(it)
                }
            }
        }
    }

    private fun subscribeToIsStored() {
        detailedClinicalTrialViewModel.asyncSubscribe(this, DetailedClinicalTrialState::isStored, onSuccess = { stored ->
            if (stored) {
                fabStoreTrial.text = "Stored"
                fabStoreTrial.extend()
            } else {
                fabStoreTrial.shrink()
            }
        })
    }

    override fun invalidate(): Unit = withState(detailedClinicalTrialViewModel) { state ->
        when (state.clinicalTrial) {
            is Success -> {
                Log.i("Success", state.clinicalTrial.invoke()!!.briefTitle)
            }
            is Fail -> {
                Log.i("Fail", state.clinicalTrial.error.localizedMessage)
            }
        }
    }

    private fun setOnClickListeners() {
        buttonScientificSummaryDetail.setOnClickListener {
            if (bottomSheetBehaviour.state == BottomSheetBehavior.STATE_COLLAPSED || bottomSheetBehaviour.state == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheetBehaviour.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        buttonTrialLocations.setOnClickListener {
            withState(detailedClinicalTrialViewModel) { state ->
                findNavController().navigate(
                    DetailedClinicalTrialFragmentDirections.actionDetailedClinicalTrialFragmentToLocationsFragment(
                        state.clinicalTrial.invoke()!!.nciID?.let { it } ?: state.clinicalTrial.invoke()!!.nctID!!
                    )
                )
            }
        }
        fabStoreTrial.setOnClickListener {
            withState(detailedClinicalTrialViewModel) { state ->
                val trial = state.clinicalTrial.invoke()!!
                if (state.isStored.invoke()!!) {
                    trial.nciID?.let {
                        detailedClinicalTrialViewModel.deleteTrial(it)
                    }
                    trial.nctID?.let {
                        detailedClinicalTrialViewModel.deleteTrial(it)
                    }
                } else {
                    when {
                        trial.nciID != null -> detailedClinicalTrialViewModel.storeArticle(
                            ArchivedArticle(
                                articleId = trial.nciID,
                                title = trial.briefTitle!!,
                                timestamp = DateTime.now().millis
                            )
                        )
                        trial.nctID != null -> detailedClinicalTrialViewModel.storeArticle(
                            ArchivedArticle(
                                articleId = trial.nctID,
                                title = trial.briefTitle!!,
                                timestamp = DateTime.now().millis
                            )
                        )
                        else -> {
                            // do nothing
                        }
                    }
                }
            }
        }
        ivBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun subscribeToClinicalTrial() {
        detailedClinicalTrialViewModel.asyncSubscribe(DetailedClinicalTrialState::clinicalTrial) { state ->
            updateViews(state)
            setOnClickListeners()
        }
    }

    private fun updateViews(trials: ClinicalTrials.Trials) {
        searchQuery?.let {
            val queryWordList = it.split(" ")
            val titleSpannable = SpannableString(trials.briefTitle)
            queryWordList.forEach { splitString ->
                val indexOfQuery = trials.briefTitle?.toLowerCase()?.indexOf(splitString.toLowerCase())
                indexOfQuery?.let { index ->
                    if (index > -1) {
                        titleSpannable.setSpan(
                            UnderlineSpan(),
                            index, index + splitString.length,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                    }
                }
            }
            textBriefTitle.text = titleSpannable

            trials.briefSummary?.let { summary ->
                val briefSummarySpannable = SpannableString(summary)
                queryWordList.forEach { splitString ->
                    val indexOfQuery = summary.toLowerCase().indexOf(splitString.toLowerCase())
                    indexOfQuery?.let { index ->
                        if (index > -1) {
                            briefSummarySpannable.setSpan(
                                ForegroundColorSpan(requireContext().resources.getColor(R.color.chip_color_biomarkers)),
                                index, index + splitString.length,
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                            )
                        }
                    }
                }
                compoundTrialSummary.setSpannableData(briefSummarySpannable)
            }
        } ?: run {
            textBriefTitle.text = trials.briefTitle
            trials.briefSummary?.let {
                compoundTrialSummary.setData(it)
            }
        }

        compoundTrial.setData(trials.mapToStudySummary(), trials.sites.size)
        compoundTrialEligibility.setElegibilityData(trials.mapToEligibilityCriteria())
        val associated = Associated(trials.diseases, trials.biomarkers)
        trials.diseases?.let {
            compoundAssociatedDiseases.showAssociatedDiseases(associated)
        }
        trials.detailedSummary?.let {
            textScientificDescription.text = it
        } ?: run {
            buttonScientificSummaryDetail.visibility == View.GONE
        }
        clLoadingSplash.visibility = View.INVISIBLE
    }

}