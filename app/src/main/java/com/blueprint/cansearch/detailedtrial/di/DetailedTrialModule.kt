package com.blueprint.cansearch.detailedtrial.di

import androidx.room.Room
import com.blueprint.cansearch.common.domain.AppDatabase
import com.blueprint.cansearch.detailedtrial.DetailedClinicalTrialState
import com.blueprint.cansearch.detailedtrial.DetailedClinicalTrialViewModel
import com.blueprint.cansearch.detailedtrial.data.DetailedTrialRepository
import com.blueprint.cansearch.detailedtrial.ui.*
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val detailedTrialModule = module {
    single {
        DetailedTrialRepository(
            apiServices = get(),
            archivedDao = Room.databaseBuilder(
                androidApplication(),
                AppDatabase::class.java, "archivedarticle"
            ).build()
        )
    }
    single { DetailedClinicalTrialUseCases(repository = get()) }
    single { StoreClinicalTrialIdUseCase(repository = get()) }
    single { RetrieveAllTrialIdUseCase(repository = get()) }
    single { DeleteTrialUseCase(repository = get()) }
    single { IsStoredLocallyUseCase(retrieveAllTrialIdUseCase = get()) }
    viewModel {
        DetailedClinicalTrialViewModel(
            initialState = DetailedClinicalTrialState(),
            detailedClinicalTrialUseCases = get(),
            storeClinicalTrialIdUseCase = get(),
            deleteTrialUseCase = get(),
            isStoredUseCase = get()
        )
    }
}