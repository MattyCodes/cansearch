package com.blueprint.cansearch.detailedtrial.ui

import android.content.Context
import android.text.SpannableString
import android.util.AttributeSet
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.blueprint.cansearch.R

class StudySummaryCompoundView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    ConstraintLayout(context, attrs, defStyleAttr) {

    var buttonScientificSummaryDetail: TextView

    init {
        inflate(context, R.layout.study_summary_compound, this)
        buttonScientificSummaryDetail = rootView.findViewById(R.id.buttonScientificSummaryDetail)
    }

    fun setData(studySummary: String) {
        rootView.findViewById<TextView>(R.id.study_summary_description).text = studySummary
    }

    fun setSpannableData(studySummary: SpannableString) {
        rootView.findViewById<TextView>(R.id.study_summary_description).text = studySummary
    }

}