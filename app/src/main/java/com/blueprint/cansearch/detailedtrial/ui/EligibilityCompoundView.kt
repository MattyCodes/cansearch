package com.blueprint.cansearch.detailedtrial.ui

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import com.blueprint.cansearch.R
import com.blueprint.cansearch.detailedtrial.domain.EligibilityCriteria

class EligibilityCompoundView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var containerEligibilitySummary: LinearLayout

    init {
        inflate(context, R.layout.eligibility_compound, this)
        containerEligibilitySummary = rootView.findViewById(R.id.containerEligibilitySummary)
    }

    fun setElegibilityData(eligibilityCriteriaList: List<EligibilityCriteria>) {
        containerEligibilitySummary.removeAllViews()
        eligibilityCriteriaList.forEach {
            val eligibilityCriteriaView = EligibilityCriteriaItemCompoundView(context)
            eligibilityCriteriaView.setEligibilityCriteria(it)
            containerEligibilitySummary.addView(eligibilityCriteriaView)
        }
    }
}