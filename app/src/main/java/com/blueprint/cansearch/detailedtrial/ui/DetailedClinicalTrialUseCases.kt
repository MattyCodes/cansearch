package com.blueprint.cansearch.detailedtrial.ui

import com.blueprint.cansearch.common.domain.ArchivedArticle
import com.blueprint.cansearch.detailedtrial.data.DetailedTrialRepository
import io.reactivex.Single

class DetailedClinicalTrialUseCases(private val repository: DetailedTrialRepository) {
    fun execute(trialId: String) = repository.retrieveClinicalTrial(trialId)
}

class StoreClinicalTrialIdUseCase(private val repository: DetailedTrialRepository) {
    fun create(archivedArticle: ArchivedArticle) : Single<String> {
        return repository.storeTrial(archivedArticle)
    }
}

class RetrieveAllTrialIdUseCase(private val repository: DetailedTrialRepository) {
    fun execute() = repository.getAll()
}

class DeleteTrialUseCase(private val repository: DetailedTrialRepository) {
    fun create(trialID: String) : Single<String> {
        return repository.delete(trialID)
    }
}

class IsStoredLocallyUseCase(private val retrieveAllTrialIdUseCase: RetrieveAllTrialIdUseCase) {
    fun execute(trialId: String) = retrieveAllTrialIdUseCase.execute().map { articles ->
        articles.any { it.articleId == trialId }
    }
}