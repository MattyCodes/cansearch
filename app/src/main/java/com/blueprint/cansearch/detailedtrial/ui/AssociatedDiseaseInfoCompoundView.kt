package com.blueprint.cansearch.detailedtrial.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.blueprint.cansearch.R
import com.blueprint.cansearch.search.domain.Associated
import com.blueprint.cansearch.search.domain.ClinicalTrials
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup

const val diseaseType = 1
const val biomarkerType = 2

class AssociatedDiseaseInfoCompoundView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var associatedDiseaseList = ArrayList<ClinicalTrials.Trials.AssociatedDisease>()
    private var associatedDiseaseSubList = ArrayList<ClinicalTrials.Trials.AssociatedDisease>()

    private var chipGroupAssociatedDiseases: ChipGroup
    private var textDiseases: TextView
    private var textBiomarkers: TextView
    private var chipDiseaseLegend: Chip
    private var chipBiomarkerLegend: Chip

    init {
        associatedDiseaseList.clear()
        associatedDiseaseSubList.clear()
        inflate(context, R.layout.associated_disease_info_compound, this)
        chipGroupAssociatedDiseases = rootView.findViewById(R.id.chipGroupAssociatedDiseases)
        textDiseases = rootView.findViewById(R.id.textDiseases)
        textBiomarkers = rootView.findViewById(R.id.textBiomarkers)
        chipDiseaseLegend = rootView.findViewById(R.id.chipDiseaseLegend)
        chipBiomarkerLegend = rootView.findViewById(R.id.chipBiomarkerLegend)
    }

    fun showAssociatedDiseases(associated: Associated) {
        if (associated.biomarkers?.isEmpty() ?: true) {
            textBiomarkers.visibility = View.GONE
            chipBiomarkerLegend.visibility = View.GONE
        } else if (associated.diseases?.isEmpty() ?: true) {
            textDiseases.visibility = View.GONE
            chipDiseaseLegend.visibility = View.GONE
        }

        chipGroupAssociatedDiseases.removeAllViews()

        associated.filteredList().shuffled().forEach {
            val chip = Chip(context)
            chip.setTextAppearance(R.style.StudySummaryItem)
            chip.chipStrokeWidth = 2f
            if (it.first == diseaseType) {
                chip.setChipBackgroundColorResource(R.color.chip_color_diseases_background)
                chip.setChipStrokeColorResource(R.color.chip_color_diseases_stroke)
                chip.setTextColor(ContextCompat.getColor(chip.context, R.color.chip_color_diseases_stroke))
            } else if (it.first == biomarkerType) {
                chip.setChipBackgroundColorResource(R.color.chip_color_biomarker_background)
                chip.setChipStrokeColorResource(R.color.chip_color_biomarker_stroke)
                chip.setTextColor(ContextCompat.getColor(chip.context, R.color.chip_color_biomarker_stroke))
            }
            chip.text = it.second
            chipGroupAssociatedDiseases.addView(chip)
        }
    }

}

fun Associated.filteredList(): ArrayList<Pair<Int, String>> {
    val associatedList = ArrayList<Pair<Int, String>>()

    this.diseases?.let {
        if (it.size > 15) {
            it.shuffled().subList(0, 14).forEach { disease ->
                associatedList.add(Pair(diseaseType, disease.associatedDiseaseName!!))
            }
        } else {
            it.forEach { disease ->
                associatedList.add(Pair(diseaseType, disease.associatedDiseaseName!!))
            }
        }
    }

    this.biomarkers?.let {
        if (it.size > 15) {
            it.shuffled().subList(0, 14).forEach { biomarker ->
                associatedList.add(Pair(biomarkerType, biomarker.name!!))
            }
        } else {
            it.forEach { biomarker ->
                associatedList.add(Pair(biomarkerType, biomarker.name!!))
            }
        }
    }

    return associatedList
}