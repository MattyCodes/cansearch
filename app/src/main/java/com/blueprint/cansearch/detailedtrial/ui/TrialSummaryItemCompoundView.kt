package com.blueprint.cansearch.detailedtrial.ui

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.blueprint.cansearch.R
import com.blueprint.cansearch.detailedtrial.domain.StudySummaryItem

class TrialSummaryItemCompoundView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var textItemTitle: TextView
    private var textItemValue: TextView

    init {
        inflate(context, R.layout.list_item_study_summary, this)
        textItemTitle = rootView.findViewById(R.id.textItemTitle)
        textItemValue = rootView.findViewById(R.id.textItemValue)
    }

    fun setSummaryItem(studySummaryItem: StudySummaryItem) {
        textItemTitle.text = studySummaryItem.title
        textItemValue.text = studySummaryItem.value
    }
}