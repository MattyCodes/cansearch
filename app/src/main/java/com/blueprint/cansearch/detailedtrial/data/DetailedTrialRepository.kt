package com.blueprint.cansearch.detailedtrial.data

import com.blueprint.cansearch.common.domain.AppDatabase
import com.blueprint.cansearch.common.domain.ArchivedArticle
import com.blueprint.cansearch.core.network.ApiServices
import com.blueprint.cansearch.search.domain.ClinicalTrials
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetailedTrialRepository(private val apiServices: ApiServices, private val archivedDao: AppDatabase) {

    fun retrieveClinicalTrial(trialId: String): Single<ClinicalTrials.Trials> {
        return apiServices.getTrial(trialId)
    }

    fun storeTrial(archivedArticle: ArchivedArticle) =
        archivedDao.articleDao().insert(archivedArticle)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .andThen(Single.just(archivedArticle.articleId))

    fun delete(trialId: String) =
        archivedDao.articleDao().deleteOne(trialId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .andThen(Single.just(trialId))

    fun getAll() = Single.create<List<ArchivedArticle>> { emitter ->
        emitter.onSuccess(archivedDao.articleDao().getAll())
    }

    fun nuke() = Single.create<Any> { emitter ->
        emitter.onSuccess(archivedDao.articleDao().nuke())
    }

}