package com.blueprint.cansearch.detailedtrial.domain

data class EligibilityCriteria(val title: String, val value: String)