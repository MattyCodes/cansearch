package com.blueprint.cansearch.detailedtrial

import android.util.Log
import com.airbnb.mvrx.*
import com.blueprint.cansearch.common.domain.ArchivedArticle
import com.blueprint.cansearch.detailedtrial.ui.*
import com.blueprint.cansearch.search.domain.ClinicalTrials
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject

data class DetailedClinicalTrialState(
    val clinicalTrial: Async<ClinicalTrials.Trials> = Uninitialized,
    val clinicalTrialStored: Async<String> = Uninitialized,
    val deleteStoredTrial: Async<String> = Uninitialized,
    val isStored: Async<Boolean> = Uninitialized
) : MvRxState

class DetailedClinicalTrialViewModel(
    val initialState: DetailedClinicalTrialState = DetailedClinicalTrialState(),
    private val detailedClinicalTrialUseCases: DetailedClinicalTrialUseCases,
    private val storeClinicalTrialIdUseCase: StoreClinicalTrialIdUseCase,
    private val deleteTrialUseCase: DeleteTrialUseCase,
    private val isStoredUseCase: IsStoredLocallyUseCase
) : BaseMvRxViewModel<DetailedClinicalTrialState>(initialState = initialState, debugMode = BuildConfig.DEBUG) {

    init {
        Log.i("Initialized:", "DetailedClinicalTrialViewModel")
        asyncSubscribe(DetailedClinicalTrialState::clinicalTrial, onSuccess = {
            if (it.nciID != null) {
                isStored(it.nciID)
            } else if (it.nctID != null) {
                isStored(it.nctID)
            }
        })
        asyncSubscribe(DetailedClinicalTrialState::clinicalTrialStored, onSuccess = {
            Log.i("Clinical Trial", "Stored successfully")
//            retrieveAllTrials()
            isStored(it)
        })
        asyncSubscribe(DetailedClinicalTrialState::deleteStoredTrial, onSuccess = {
            Log.i("Clinical Trial", "Deleted successfully")
            isStored(it)
        })
    }

    fun retrieveTrial(trialId: String) {
        detailedClinicalTrialUseCases.execute(trialId).subscribeOn(Schedulers.io()).execute { copy(clinicalTrial = it) }
    }

    fun setTrial(trial: ClinicalTrials.Trials) {
        setState {
            copy(clinicalTrial = Success(trial))
        }
    }

    private fun isStored(id: String) {
        isStoredUseCase.execute(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .execute { copy(isStored = it) }
    }

    fun storeArticle(archivedArticle: ArchivedArticle) {
        storeClinicalTrialIdUseCase
            .create(archivedArticle).execute { copy(clinicalTrialStored = it) }
    }

    fun deleteTrial(trialID: String) {
        deleteTrialUseCase
            .create(trialID)
            .execute { copy(deleteStoredTrial = it) }
    }


    companion object : MvRxViewModelFactory<DetailedClinicalTrialViewModel, DetailedClinicalTrialState> {
        override fun create(viewModelContext: ViewModelContext, state: DetailedClinicalTrialState): DetailedClinicalTrialViewModel {

            val getTrial = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<DetailedClinicalTrialUseCases>()
            }.value

            val storeTrial = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<StoreClinicalTrialIdUseCase>()
            }.value

            val deleteTrial = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<DeleteTrialUseCase>()
            }.value

            val isStored = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<IsStoredLocallyUseCase>()
            }.value

            return DetailedClinicalTrialViewModel(state, getTrial, storeTrial, deleteTrial, isStored)
        }
    }
}