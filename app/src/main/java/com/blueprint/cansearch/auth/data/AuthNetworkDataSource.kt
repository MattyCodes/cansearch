package com.blueprint.cansearch.auth.data

import com.blueprint.cansearch.auth.domain.emailUpdate
import com.blueprint.cansearch.auth.domain.passwordUpdate
import com.blueprint.cansearch.auth.domain.rxCreateUserWithEmailAndPassword
import com.blueprint.cansearch.auth.domain.rxSignInWithEmailAndPassword
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Single

class AuthNetworkDataSource(
    val firebaseAuth: FirebaseAuth
) {
    fun login(email: String, password: String): Single<AuthResult> = firebaseAuth
        .rxSignInWithEmailAndPassword(email, password)

    fun signUp(email: String, password: String) = firebaseAuth
        .rxCreateUserWithEmailAndPassword(email, password)

    fun updateEmailAddress(email: String) =firebaseAuth.currentUser!!.emailUpdate(email)

    fun changePassword(email: String) =firebaseAuth.currentUser!!.passwordUpdate(email)
}