package com.blueprint.cansearch.auth.ui

import com.blueprint.cansearch.settings.SettingsState
import com.blueprint.cansearch.settings.SettingsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val settingsModule = module {

    single { UpdateUserEmailUseCase(repository = get()) }
    single { UpdateUserPasswordUseCase(repository = get()) }
    single {
        DeleteAccountUseCase(
            authRepository = get(),
            localDataBase = get()
        )
    }

    viewModel {
        SettingsViewModel(
            initialState = SettingsState(),
            updateUserEmailUseCase = get(),
            updateUserPasswordUseCase = get(),
            deleteAccountUseCase = get()
        )
    }
}