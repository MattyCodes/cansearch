package com.blueprint.cansearch.auth.ui

import com.airbnb.mvrx.*
import com.google.firebase.auth.AuthResult
import org.koin.android.ext.android.inject

data class AuthState(
    val login: Async<AuthResult> = Uninitialized,
    val signup: Async<AuthResult> = Uninitialized
) : MvRxState

data class AuthRequest(
    val email: String,
    val password: String
)

class AuthViewModel(initialState: AuthState = AuthState(),
                    private val signupUseCase: SignupUseCase,
                    private val loginUseCase: LoginUseCase) :
    BaseMvRxViewModel<AuthState>(initialState, debugMode = BuildConfig.DEBUG) {

    fun signup(authRequest: AuthRequest) {
        signupUseCase.create(authRequest)
            .execute { copy(signup = it) }
    }

    fun login(authRequest: AuthRequest) {
        loginUseCase.create(authRequest)
            .execute { copy(login = it) }
    }

    companion object : MvRxViewModelFactory<AuthViewModel, AuthState> {
        override fun create(viewModelContext: ViewModelContext, state: AuthState): AuthViewModel {

            val signupUseCase = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<SignupUseCase>()
            }.value

            val loginUseCase = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<LoginUseCase>()
            }.value

            return AuthViewModel(state, signupUseCase, loginUseCase)
        }
    }

}