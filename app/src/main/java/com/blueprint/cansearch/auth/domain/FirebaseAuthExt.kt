package com.blueprint.cansearch.auth.domain

import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import io.reactivex.Single

fun FirebaseAuth.rxSignInWithEmailAndPassword(email: String, password: String): Single<AuthResult> {
    return Single.create { emitter ->
        signInWithEmailAndPassword(email, password).apply {
            addOnSuccessListener {
                emitter.onSuccess(it)
            }
            addOnFailureListener {
                emitter.onError(it)
            }
        }
    }
}

fun FirebaseAuth.rxCreateUserWithEmailAndPassword(email: String, password: String): Single<AuthResult> {
    return Single.create { emitter ->
        createUserWithEmailAndPassword(email, password).apply {
            addOnSuccessListener {
                emitter.onSuccess(it)
            }
            addOnFailureListener {
                emitter.onError(it)
            }
        }
    }
}

fun FirebaseUser.emailUpdate(email: String): Single<Boolean> = Single.create { emitter ->
    updateEmail(email).apply {
        addOnSuccessListener {
            emitter.onSuccess(true)
        }
        addOnFailureListener {
            emitter.onError(it)
        }
    }
}

fun FirebaseUser.passwordUpdate(password: String): Single<Boolean> = Single.create { emitter ->
    updatePassword(password).apply {
        addOnSuccessListener {
            emitter.onSuccess(true)
        }
        addOnFailureListener {
            emitter.onError(it)
        }
    }
}