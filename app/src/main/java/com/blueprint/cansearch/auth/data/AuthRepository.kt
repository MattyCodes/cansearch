package com.blueprint.cansearch.auth.data

import io.reactivex.Single


class AuthRepository(private val authNetworkDataSource: AuthNetworkDataSource) {
    fun login(email: String, password: String) = authNetworkDataSource.login(email, password)
    fun signUp(email: String, password: String) = authNetworkDataSource.signUp(email, password)
    fun changeEmail(email: String) = authNetworkDataSource.updateEmailAddress(email)
    fun changePassword(email: String) = authNetworkDataSource.changePassword(email)
    fun deleteAccount() = Single.create<Boolean> { emitter ->
        authNetworkDataSource.firebaseAuth.currentUser!!.delete().addOnSuccessListener {
            emitter.onSuccess(true)
        }.addOnFailureListener {
            emitter.onError(it)
        }
    }
}