package com.blueprint.cansearch.auth.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.blueprint.cansearch.R

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
    }
}