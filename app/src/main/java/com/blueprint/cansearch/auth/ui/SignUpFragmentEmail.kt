package com.blueprint.cansearch.auth.ui

import android.animation.ObjectAnimator
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.blueprint.cansearch.R
import com.blueprint.cansearch.auth.domain.isValidEmail
import com.jakewharton.rxbinding3.widget.textChanges

const val signUpEmailKey = "email"

class SignUpFragmentEmail : Fragment() {

    // views
    private lateinit var editTextEmail: EditText
    private lateinit var textEmailNext: TextView
    private lateinit var textEmailErrorPrompt: TextView

    // animator properties
    private lateinit var errorPromptAnimator: ObjectAnimator

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sing_up_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editTextEmail = view.findViewById(R.id.editTextEmail)
        textEmailNext = view.findViewById(R.id.textEmailNext)
        textEmailErrorPrompt = view.findViewById(R.id.textEmailErrorPrompt)
        instantiateEmailErrorPromptAnimator()
        setTextWatcherOnEmailInput()
        setOnClickListener()
    }

    private fun setOnClickListener() {
        textEmailNext.setOnClickListener {
            val emailAddressInput = editTextEmail.text.toString()
            if (emailAddressInput.isNotEmpty() && emailAddressInput.isValidEmail()) {
                findNavController().navigate(
                    SignUpFragmentEmailDirections.actionSignUpFragmentEmail2ToSignUpFragmentPassword2(
                        emailAddressInput
                    )
                )
            } else {
                showEmailAddressErrorPrompt()
            }
        }
    }

    private fun instantiateEmailErrorPromptAnimator() {
        errorPromptAnimator = ObjectAnimator
            .ofFloat(textEmailErrorPrompt, View.ALPHA, 1f, 0f)
            .apply {
                startDelay = 10000
                duration = 500
                interpolator = AccelerateDecelerateInterpolator()
            }
    }

    private fun setTextWatcherOnEmailInput() {
        editTextEmail.textChanges()
            .subscribe({
                if (it.isNotEmpty()) {
                    if (it.isValidEmail()) {
                        updateNavigationCTAColor(true)
                        hideEmailErrorPrompt()
                    } else {
                        updateNavigationCTAColor(false)
                        showEmailAddressErrorPrompt()
                    }
                }
            }, {
                Log.e("Base input error", it.toString())
            })
    }

    private fun updateNavigationCTAColor(allowNav: Boolean) {
        if (allowNav) {
            textEmailNext.setTextColor(ContextCompat.getColor(requireContext(), R.color.sign_up_navigate_enabled))
        } else {
            textEmailNext.setTextColor(ContextCompat.getColor(requireContext(), R.color.sign_up_navigate_disabled))
        }
    }

    private fun hideEmailErrorPrompt() {
        errorPromptAnimator.cancel()
        textEmailErrorPrompt.alpha = 0f
    }

    private fun showEmailAddressErrorPrompt() {
        textEmailErrorPrompt.alpha = 1f
        errorPromptAnimator.start()
    }

}