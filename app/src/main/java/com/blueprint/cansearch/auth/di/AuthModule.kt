package com.blueprint.cansearch.auth.di

import com.blueprint.cansearch.auth.data.AuthNetworkDataSource
import com.blueprint.cansearch.auth.data.AuthRepository
import com.blueprint.cansearch.auth.ui.AuthState
import com.blueprint.cansearch.auth.ui.AuthViewModel
import com.blueprint.cansearch.auth.ui.LoginUseCase
import com.blueprint.cansearch.auth.ui.SignupUseCase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val authModule = module {
    single { AuthNetworkDataSource(firebaseAuth = get()) }
    single { AuthRepository(authNetworkDataSource = get()) }
    single { SignupUseCase(repository = get()) }
    single { LoginUseCase(repository = get()) }
    viewModel {
        AuthViewModel(
            initialState = AuthState(),
            signupUseCase = get(),
            loginUseCase = get()
        )
    }
}