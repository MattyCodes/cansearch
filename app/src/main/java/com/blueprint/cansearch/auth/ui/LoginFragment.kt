package com.blueprint.cansearch.auth.ui

import android.animation.ObjectAnimator
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.airbnb.mvrx.*
import com.blueprint.cansearch.R
import com.blueprint.cansearch.auth.domain.isValidEmail
import com.google.android.material.button.MaterialButton
import com.jakewharton.rxbinding3.widget.textChanges

const val loginFragTag = "TAG***_login"

class LoginFragment : BaseMvRxFragment() {

    private val authViewModel: AuthViewModel by fragmentViewModel()

    private lateinit var buttonLoginCTA: MaterialButton
    private lateinit var textSignUp: TextView
    private lateinit var textEmailErrorPrompt: TextView
    private lateinit var textPasswordErrorPrompt: TextView
    private lateinit var textServerErrorPrompt: TextView
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var loginLoadingScreen: ConstraintLayout

    // animation variables
    private lateinit var emailErrorPromptAnimator: ObjectAnimator
    private lateinit var passwordErrorPromptAnimator: ObjectAnimator

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonLoginCTA = view.findViewById(R.id.buttonLoginCTA)
        textSignUp = view.findViewById(R.id.textSignUp)
        editTextEmail = view.findViewById(R.id.editTextEmail)
        editTextPassword = view.findViewById(R.id.editTextPassword)
        textEmailErrorPrompt = view.findViewById(R.id.textEmailErrorPrompt)
        textPasswordErrorPrompt = view.findViewById(R.id.textPasswordErrorPrompt)
        textServerErrorPrompt = view.findViewById(R.id.textServerErrorPrompt)
        loginLoadingScreen = view.findViewById(R.id.loginLoadingScreen)
        setOnClickListeners()
        instantiateErrorPromptAnimators()
        setTextWatchers()
    }

    override fun invalidate(): Unit = withState(authViewModel) { state ->
        when (state.login) {
            is Loading -> {
                Log.i(loginFragTag, createUserLoading)
                loginLoadingScreen.visibility = View.VISIBLE
            }
            is Success -> {
                Log.i(loginFragTag, createUserSuccess)
                findNavController().navigate(R.id.action_loginFragment2_to_mainActivity2)
            }
            is Fail -> {
                loginLoadingScreen.visibility = View.GONE
                textServerErrorPrompt.text = state.login.error.message
                textServerErrorPrompt.alpha = 1f
                Log.i(loginFragTag, state.login.error.message)
            }
        }
    }

    private fun setTextWatchers() {
        editTextEmail.textChanges()
            .subscribe({
                if (emailErrorPromptAnimator.isStarted){
                    cancelErrorPromptAnimation(emailErrorPromptAnimator, textEmailErrorPrompt)
                }
            }, {

            })
        editTextPassword.textChanges()
            .subscribe({
                if (passwordErrorPromptAnimator.isStarted){
                    cancelErrorPromptAnimation(passwordErrorPromptAnimator, textPasswordErrorPrompt)
                }
            }, {

            })
    }

    private fun instantiateErrorPromptAnimators() {
        emailErrorPromptAnimator = ObjectAnimator
            .ofFloat(textEmailErrorPrompt, View.ALPHA, 1f, 0f)
            .apply {
                startDelay = 10000
                duration = 500
                interpolator = AccelerateDecelerateInterpolator()
            }

        passwordErrorPromptAnimator = ObjectAnimator
            .ofFloat(textPasswordErrorPrompt, View.ALPHA, 1f, 0f)
            .apply {
                startDelay = 10000
                duration = 500
                interpolator = AccelerateDecelerateInterpolator()
            }
    }

    private fun setOnClickListeners() {
        buttonLoginCTA.setOnClickListener {
            validateEmail()
        }
        textSignUp.setOnClickListener {
            it.findNavController().navigate(R.id.action_loginFragment2_to_signUpFragmentEmail2)
        }
    }

    private fun validateEmail() {
        val emailAddressInput = editTextEmail.text.toString()
        when {
            emailAddressInput.isEmpty() || !emailAddressInput.isValidEmail() -> {
                showErrorPromptAnimation(emailErrorPromptAnimator, textEmailErrorPrompt)
            }
            else -> { validatePassword() }
        }
    }

    private fun validatePassword() {
        val passwordInput = editTextPassword.text.toString()
        when {
            passwordInput.isEmpty() || passwordInput.length < 5 -> {
                showErrorPromptAnimation(passwordErrorPromptAnimator, textPasswordErrorPrompt)
            }
            else -> {
                authViewModel.login(AuthRequest(editTextEmail.text.toString(), editTextPassword.text.toString()))
            }
        }
    }

    private fun showErrorPromptAnimation(errorPromptAnimator: ObjectAnimator, view: View){
        view.alpha = 1f
        errorPromptAnimator.start()
    }

    private fun cancelErrorPromptAnimation(errorPromptAnimator: ObjectAnimator, view: View){
        errorPromptAnimator.cancel()
        view.alpha = 0f
    }
}