package com.blueprint.cansearch.auth.ui

import android.animation.ObjectAnimator
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.transition.ChangeBounds
import androidx.transition.TransitionManager
import com.airbnb.mvrx.*
import com.blueprint.cansearch.R
import com.jakewharton.rxbinding3.widget.textChanges

const val createUserFragTag = "TAG***_create_user"
const val createUserSuccess = "::: SUCCESS"
const val createUserFail = "::: FAIL"
const val createUserLoading = "::: LOADING"

class SignUpFragmentPassword : BaseMvRxFragment() {

    private val authViewModel: AuthViewModel by fragmentViewModel()

    // auth variables
    private lateinit var email: String

    // views
    private lateinit var root: ConstraintLayout
    private lateinit var clSignUpSplash: ConstraintLayout
    private lateinit var editTextPassword: EditText
    private lateinit var textPasswordErrorPrompt: TextView
    private lateinit var textPasswordNext: TextView

    // animator properties
    private lateinit var errorPromptAnimator: ObjectAnimator
    private val splashScreenConstraint = ConstraintSet()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        email = arguments?.getString(signUpEmailKey) ?: ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_up_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        root = view.findViewById(R.id.root)
        clSignUpSplash = view.findViewById(R.id.clSignUpSplash)
        editTextPassword = view.findViewById(R.id.editTextPassword)
        textPasswordNext = view.findViewById(R.id.textPasswordNext)
        textPasswordErrorPrompt = view.findViewById(R.id.textPasswordErrorPrompt)
        instantiatePassportErrorPromptAnimator()
        setTextWatcherOnPasswordInput()
        instantiateSignUpSplashScreenConstraints()
        setOnClickListener()
    }

    override fun invalidate(): Unit = withState(authViewModel) { state ->
        when (state.signup) {
            is Loading -> {
                Log.i(createUserFragTag, createUserLoading)
                animateInSplashScreen()
            }
            is Success -> {
                Log.i(createUserFragTag, createUserSuccess)
                Handler().postDelayed({
                    proceedNewUserJourney()
                }, 750)
            }
            is Fail -> {
                Log.i(createUserFragTag, createUserFail)
                textPasswordNext.visibility = View.VISIBLE
                clSignUpSplash.visibility = View.INVISIBLE
            }
        }
    }

    private fun proceedNewUserJourney() {
        findNavController().navigate(R.id.action_signUpFragmentPassword2_to_onBoardingFragment2)
//        activity?.let { it.finish() }
    }

    private fun instantiateSignUpSplashScreenConstraints() {
        splashScreenConstraint.clone(root)
        splashScreenConstraint.connect(clSignUpSplash.id, ConstraintSet.START, root.id, ConstraintSet.START, 0)
        splashScreenConstraint.connect(clSignUpSplash.id, ConstraintSet.END, root.id, ConstraintSet.END, 0)
        splashScreenConstraint.connect(clSignUpSplash.id, ConstraintSet.TOP, root.id, ConstraintSet.TOP, 0)
        splashScreenConstraint.connect(clSignUpSplash.id, ConstraintSet.BOTTOM, root.id, ConstraintSet.BOTTOM, 0)
        splashScreenConstraint.setVisibility(clSignUpSplash.id, View.VISIBLE)
        splashScreenConstraint.setVisibility(textPasswordNext.id, View.GONE)
    }

    private fun setTextWatcherOnPasswordInput() {
        editTextPassword.textChanges()
            .subscribe({
                if (it.isNotEmpty()) {
                    if (it.length >= 5) {
                        updateNavigationCTAColor(true)
                        hidePasswordErrorPrompt()
                    } else {
                        updateNavigationCTAColor(false)
                        showPasswordErrorPrompt()
                    }
                }
            }, {
                Log.e("Base input error", it.toString())
            })
    }

    private fun instantiatePassportErrorPromptAnimator() {
        errorPromptAnimator = ObjectAnimator
            .ofFloat(textPasswordErrorPrompt, View.ALPHA, 1f, 0f)
            .apply {
                startDelay = 10000
                duration = 500
                interpolator = AccelerateDecelerateInterpolator()
            }
    }

    private fun setOnClickListener() {
        textPasswordNext.setOnClickListener {
            val passwordInput = editTextPassword.text.toString()
            if (passwordInput.isNotEmpty() && passwordInput.length >= 5) {
                proceedWithSignIn(passwordInput)
            } else {
                showPasswordErrorPrompt()
            }
        }
    }

    private fun animateInSplashScreen() {
        val transition = ChangeBounds().apply {
            duration = 175
            interpolator = AccelerateDecelerateInterpolator()
        }
        TransitionManager.beginDelayedTransition(root, transition)
        splashScreenConstraint.applyTo(root)
    }

    private fun proceedWithSignIn(passwordInput: String) {
        authViewModel.signup(AuthRequest(email, passwordInput))
    }

    private fun updateNavigationCTAColor(allowNav: Boolean) {
        if (allowNav) {
            textPasswordNext.setTextColor(ContextCompat.getColor(requireContext(), R.color.sign_up_navigate_enabled))
        } else {
            textPasswordNext.setTextColor(ContextCompat.getColor(requireContext(), R.color.sign_up_navigate_disabled))
        }
    }

    private fun hidePasswordErrorPrompt() {
        errorPromptAnimator.cancel()
        textPasswordErrorPrompt.alpha = 0f
    }

    private fun showPasswordErrorPrompt() {
        textPasswordErrorPrompt.alpha = 1f
        errorPromptAnimator.start()
    }

}
