package com.blueprint.cansearch.auth.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.blueprint.cansearch.R
import com.google.firebase.auth.FirebaseAuth

class SplashScreenFragment : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_splash_screen, container, false)
    }

    override fun onResume() {
        super.onResume()
        FirebaseAuth.getInstance().currentUser?.let {
            findNavController().navigate(R.id.action_splashScreenFragment2_to_mainActivity2)
            activity?.let {
                it.finish()
            }
        } ?: run {
            findNavController().navigate(R.id.action_splashScreenFragment2_to_loginFragment2)
        }
    }
}