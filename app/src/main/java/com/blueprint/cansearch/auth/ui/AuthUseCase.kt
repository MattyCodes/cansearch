package com.blueprint.cansearch.auth.ui

import com.blueprint.cansearch.auth.data.AuthRepository
import com.blueprint.cansearch.common.domain.AppDatabase
import com.blueprint.cansearch.detailedtrial.data.DetailedTrialRepository
import com.google.firebase.auth.AuthResult
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class SignupUseCase(private val repository: AuthRepository) {
    fun create(authRequest: AuthRequest): Single<AuthResult> {
        return repository.signUp(authRequest.email, authRequest.password)
    }
}

class LoginUseCase(private val repository: AuthRepository) {
    fun create(authRequest: AuthRequest): Single<AuthResult> {
        return repository.login(authRequest.email, authRequest.password)
    }
}

class UpdateUserEmailUseCase(private val repository: AuthRepository) {
    fun create(email: String): Single<Boolean> {
        return repository.changeEmail(email)
    }
}

class UpdateUserPasswordUseCase(private val repository: AuthRepository) {
    fun create(password: String): Single<Boolean> {
        return repository.changePassword(password)
    }
}

class DeleteAccountUseCase(private val authRepository: AuthRepository, private val localDataBase: DetailedTrialRepository){
    private val deleteLocalData = localDataBase.nuke().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    private val deleteFromAuthRepo = authRepository.deleteAccount()

    fun create() : Single<Any>{
        return deleteLocalData.flatMap {
            deleteFromAuthRepo
        }
    }

    fun deleteLocalDataBase() = deleteLocalData
}


