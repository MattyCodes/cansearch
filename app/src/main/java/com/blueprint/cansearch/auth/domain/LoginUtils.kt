package com.blueprint.cansearch.auth.domain

import android.util.Patterns

fun CharSequence.isValidEmail() = Patterns.EMAIL_ADDRESS.matcher(this).matches()
fun CharSequence.isPasswordValid() = length > 5