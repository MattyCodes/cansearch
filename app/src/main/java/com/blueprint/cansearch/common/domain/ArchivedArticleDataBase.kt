package com.blueprint.cansearch.common.domain

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(ArchivedArticle::class), version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun articleDao(): ArchivedDao
}