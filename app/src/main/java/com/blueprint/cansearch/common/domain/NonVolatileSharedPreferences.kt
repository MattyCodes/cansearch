package com.blueprint.cansearch.common.domain

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

const val NON_VOLATILE_PREFS_TITLE = "non_volatile_shared_preferences"
const val USER_ONBOARDED = "user_onboarded"

class NonVolatileSharedPreferences {

    private fun getPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences(NON_VOLATILE_PREFS_TITLE, Context.MODE_PRIVATE)

//    fun getHasUserBeenOnboarded(context: Context) = getPreferences(context).getBoolean(USER_ONBOARDED, false)
//    fun storeHasUserBeenOnboarded(context: Context, enabled: Boolean) = getPreferences(context).edit { putBoolean(USER_ONBOARDED, enabled) }

}