package com.blueprint.cansearch.common.domain

import androidx.room.*
import io.reactivex.Completable

@Dao
interface ArchivedDao {
    @Query("SELECT * FROM archive_table")
    fun getAll(): List<ArchivedArticle>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(archivedArticle: ArchivedArticle) : Completable

    @Query("DELETE FROM archive_table WHERE articleID = :articleID")
    fun deleteOne(articleID: String) : Completable


    @Query("DELETE FROM archive_table")
    fun nuke()

    @Delete
    fun delete(archivedArticle: ArchivedArticle)
}