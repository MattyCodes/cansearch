package com.blueprint.cansearch.common.domain

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "archive_table")
data class ArchivedArticle(
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "articleID") val articleId: String,
    @NonNull
    @ColumnInfo(name = "title") val title: String,
    @NonNull
    @ColumnInfo(name = "timestamp") val timestamp: Long)
