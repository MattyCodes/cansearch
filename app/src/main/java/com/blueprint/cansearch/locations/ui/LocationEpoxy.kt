package com.blueprint.cansearch.locations.ui

import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.blueprint.cansearch.R
import com.blueprint.cansearch.common.ui.KotlinEpoxyHolder
import com.blueprint.cansearch.locations.domain.LocationDetail

@EpoxyModelClass(layout = R.layout.list_item_location)
abstract class LocationEpoxy : EpoxyModelWithHolder<LocationEpoxyModelHolder>() {

    @EpoxyAttribute
    lateinit var locationDetail: LocationDetail

    @EpoxyAttribute
    lateinit var onLocationClickListener: OnLocationClickListener

    override fun bind(holder: LocationEpoxyModelHolder) {
        with(holder) {
            root.findViewById<TextView>(R.id.textLocationInstituteTitle).text = locationDetail.orgName
            root.setOnClickListener {
                onLocationClickListener.onLocationSelected(locationDetail)
            }
        }
    }
}

class LocationEpoxyModelHolder : KotlinEpoxyHolder() {
    val root by bind<ConstraintLayout>(R.id.root)
}