package com.blueprint.cansearch.locations.domain

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.parcel.Parcelize

open class Location
data class CountryHeader(val countryName: String) : Location()
data class ProvinceHeader(val cityName: String) : Location()
@Parcelize
data class LocationDetail(
    val orgName: String?,
    val addressLine: String?,
    val orgCity: String?,
    val orgPostalCode: String?,
    val orgStateOrProvince: String?,
    val orgCountry: String?,
    val recruitmentStatus: String?,
    val contactEmail: String?,
    val orgPhoneme: String?,
    val latLong: LatLng?
) : Location(), Parcelable

