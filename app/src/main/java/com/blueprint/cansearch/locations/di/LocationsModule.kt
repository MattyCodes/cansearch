package com.blueprint.cansearch.locations.di

import com.blueprint.cansearch.locations.LocationsTrialState
import com.blueprint.cansearch.locations.LocationsViewModel
import com.blueprint.cansearch.locations.ui.OrderLocationsUseCase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val locationModule = module {

    single { OrderLocationsUseCase() }

    viewModel {
        LocationsViewModel(
            initialState = LocationsTrialState(),
            detailedClinicalTrialUseCases = get(),
            orderLocationsUseCase = get()
        )
    }
}