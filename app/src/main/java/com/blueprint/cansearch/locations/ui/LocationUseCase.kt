package com.blueprint.cansearch.locations.ui

import com.blueprint.cansearch.locations.domain.CountryHeader
import com.blueprint.cansearch.locations.domain.Location
import com.blueprint.cansearch.locations.domain.LocationDetail
import com.blueprint.cansearch.locations.domain.ProvinceHeader
import com.blueprint.cansearch.search.domain.ClinicalTrials
import com.google.android.gms.maps.model.LatLng

class OrderLocationsUseCase {
    fun execute(locationList: List<ClinicalTrials.Trials.SitesItem>): List<Location> {
        val list = ArrayList<Location>()
        val orderedCountry = locationList.sortedBy { it.orgCountry }.groupBy { it.orgCountry }
        orderedCountry.forEach { grouped ->
            grouped.key?.let { header ->
                list.add(CountryHeader(header))
                grouped.value.sortedBy { it.orgStateOrProvince }.groupBy { it.orgStateOrProvince }.forEach {
                    it.key?.let { province ->
                        list.add(ProvinceHeader(province))
                    }
                    it.value.sortedBy { it.orgName }.forEach {
                        list.add(
                            LocationDetail(
                                orgName = it.orgName,
                                addressLine = it.orgAddressLine1,
                                orgCity = it.orgCity,
                                orgPostalCode = it.orgPostalCode,
                                orgStateOrProvince = it.orgStateOrProvince,
                                orgCountry = it.orgCountry,
                                recruitmentStatus = it.recruitmentStatus,
                                contactEmail = it.contactEmail,
                                orgPhoneme = it.orgPhone,
                                latLong = it.orgCoordinates?.let { LatLng(it.lat, it.lon) } ?: null
                            )
                        )
                    }
                }
            }
        }
        return list.toList()
    }
}