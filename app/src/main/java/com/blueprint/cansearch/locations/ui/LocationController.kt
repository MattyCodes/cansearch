package com.blueprint.cansearch.locations.ui

import com.airbnb.epoxy.TypedEpoxyController
import com.blueprint.cansearch.locations.domain.CountryHeader
import com.blueprint.cansearch.locations.domain.Location
import com.blueprint.cansearch.locations.domain.LocationDetail
import com.blueprint.cansearch.locations.domain.ProvinceHeader

class LocationController(val onLocationClickListener: OnLocationClickListener) : TypedEpoxyController<List<Location>>() {
    override fun buildModels(location: List<Location>) {
        location.forEachIndexed { _, location ->
            when (location) {
                is CountryHeader -> {
                    countryHeader {
                        id(location.countryName)
                        countryHeader(location)
                    }
                }
                is ProvinceHeader -> {
                    provinceSubheader {
                        id(location.cityName)
                        provinceHeader(location)
                    }
                }
                is LocationDetail -> {
                    location {
                        id("${location.orgName}${location.orgPostalCode}")
                        locationDetail(location)
                        onLocationClickListener(onLocationClickListener)
                    }
                }
            }
        }
    }
}

interface OnLocationClickListener {
    fun onLocationSelected(location: Location)
}