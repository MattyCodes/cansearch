package com.blueprint.cansearch.locations.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.mvrx.BaseMvRxFragment
import com.airbnb.mvrx.fragmentViewModel
import com.blueprint.cansearch.R
import com.blueprint.cansearch.locations.*
import com.blueprint.cansearch.locations.domain.Location
import com.blueprint.cansearch.locations.domain.LocationDetail
import com.jakewharton.rxbinding3.widget.textChanges
import java.util.concurrent.TimeUnit

class LocationsFragment : BaseMvRxFragment(), OnLocationClickListener {

    private val locationsViewModel: LocationsViewModel by fragmentViewModel()

    // id
    private lateinit var trialId: String

    private lateinit var locationsController: LocationController
    private lateinit var recyclerViewSearchLocations: RecyclerView
    private lateinit var editTextSearchLocationsInput: EditText
    private lateinit var textLocationsNumber: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trialId = arguments?.get("trialId").toString()
        locationsViewModel.retrieveTrial(trialId)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_locations, container, false)
        recyclerViewSearchLocations = root.findViewById(R.id.recyclerViewSearchLocations)
        editTextSearchLocationsInput = root.findViewById(R.id.editTextSearchLocationsInput)
        textLocationsNumber = root.findViewById(R.id.textLocationsNumber)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        locationsController = LocationController(this)
        setupRecyclerView()
        setupEditTextWatcher()
    }

    override fun onResume() {
        super.onResume()
        subscribeToFullList()
        subscribeToLocationsList()
    }

    private fun subscribeToFullList() {
        locationsViewModel.asyncSubscribe(this, LocationsTrialState::fullList) { state ->
            textLocationsNumber.text = "${state.size} sites available"
        }
    }

    override fun onLocationSelected(location: Location) {
        findNavController().navigate(
            LocationsFragmentDirections.actionLocationsFragmentToSitesFragment(
                location = location as LocationDetail
            )
        )
    }

    private fun setupEditTextWatcher() {
        editTextSearchLocationsInput
            .textChanges()
            .throttleLast(300, TimeUnit.MILLISECONDS)
            .subscribe({
                if (it.isNotEmpty()) {
                    locationsViewModel.filterLocations(it.toString())
                } else {
                    locationsViewModel.showAll()
                }
            }, {
                Log.e("Base input error", it.toString())
            })
    }

    private fun setupRecyclerView() {
        recyclerViewSearchLocations.adapter = locationsController.adapter
        recyclerViewSearchLocations.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun subscribeToLocationsList() {
        locationsViewModel.asyncSubscribe(this, LocationsTrialState::locationList) { state ->
            locationsController.setData(state)
        }
    }

    override fun invalidate() {

    }

}