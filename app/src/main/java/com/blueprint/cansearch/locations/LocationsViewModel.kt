package com.blueprint.cansearch.locations

import com.airbnb.mvrx.*
import com.blueprint.cansearch.detailedtrial.ui.DetailedClinicalTrialUseCases
import com.blueprint.cansearch.locations.domain.Location
import com.blueprint.cansearch.locations.ui.OrderLocationsUseCase
import com.blueprint.cansearch.search.domain.ClinicalTrials
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject

data class LocationsTrialState(
    val clinicalTrial: Async<ClinicalTrials.Trials> = Uninitialized,
    val fullList: Async<List<ClinicalTrials.Trials.SitesItem>> = Uninitialized,
    val locationList: Async<List<Location>> = Uninitialized
) : MvRxState

data class LocationsViewModel(
    val initialState: LocationsTrialState = LocationsTrialState(),
    private val detailedClinicalTrialUseCases: DetailedClinicalTrialUseCases,
    private val orderLocationsUseCase: OrderLocationsUseCase
) : BaseMvRxViewModel<LocationsTrialState>(initialState = initialState, debugMode = BuildConfig.DEBUG) {

    init {
        asyncSubscribe(LocationsTrialState::clinicalTrial, onSuccess = {
            setState { copy(fullList = Success(it.sites)) }
        })
        asyncSubscribe(LocationsTrialState::fullList, onSuccess = {
            setState { copy(locationList = Success(orderLocationsUseCase.execute(it))) }
        })
    }

    fun retrieveTrial(trialId: String) {
        detailedClinicalTrialUseCases.execute(trialId).subscribeOn(Schedulers.io()).execute { copy(clinicalTrial = it) }
    }

    fun filterLocations(query: String){
        withState { state: LocationsTrialState ->
            setState { copy(locationList = Success(orderLocationsUseCase.execute(state.fullList.invoke()!!.filter { it.orgStateOrProvince!!.toLowerCase().contains(query.toLowerCase()) || it.orgCountry!!.toLowerCase().contains(query.toLowerCase()) }))) }
        }
    }

    fun showAll(){
        withState { state: LocationsTrialState ->
            state.fullList.invoke()?.let {
                setState { copy(locationList = Success(orderLocationsUseCase.execute(it))) }
            }
        }
    }

    companion object : MvRxViewModelFactory<LocationsViewModel, LocationsTrialState> {
        override fun create(viewModelContext: ViewModelContext, state: LocationsTrialState): LocationsViewModel {

            val getTrial = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<DetailedClinicalTrialUseCases>()
            }.value

            val orderLocationsUseCase = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<OrderLocationsUseCase>()
            }.value

            return LocationsViewModel(state, getTrial, orderLocationsUseCase)
        }
    }

}