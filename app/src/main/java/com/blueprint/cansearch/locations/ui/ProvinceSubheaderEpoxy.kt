package com.blueprint.cansearch.locations.ui

import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.blueprint.cansearch.R
import com.blueprint.cansearch.common.ui.KotlinEpoxyHolder
import com.blueprint.cansearch.locations.domain.ProvinceHeader

@EpoxyModelClass(layout = R.layout.list_item_province)
abstract class ProvinceSubheaderEpoxy : EpoxyModelWithHolder<ProvinceSubheaderModelHolder>() {

    @EpoxyAttribute
    lateinit var provinceHeader: ProvinceHeader

    override fun bind(holder: ProvinceSubheaderModelHolder) {
        with(holder) {
            root.findViewById<TextView>(R.id.textProvinceTitle).text = provinceHeader.cityName
        }
    }
}

class ProvinceSubheaderModelHolder : KotlinEpoxyHolder() {
    val root by bind<ConstraintLayout>(R.id.root)
}