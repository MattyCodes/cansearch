package com.blueprint.cansearch.locations.ui

import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.blueprint.cansearch.R
import com.blueprint.cansearch.common.ui.KotlinEpoxyHolder
import com.blueprint.cansearch.locations.domain.CountryHeader

@EpoxyModelClass(layout = R.layout.list_item_country_header)
abstract class CountryHeaderEpoxyModel : EpoxyModelWithHolder<CountryHeaderEpoxyModelHolder>() {

    @EpoxyAttribute
    lateinit var countryHeader: CountryHeader

    override fun bind(holder: CountryHeaderEpoxyModelHolder) {
        with(holder) {
            root.findViewById<TextView>(R.id.textCountryTitle).text = countryHeader.countryName
        }
    }
}

class CountryHeaderEpoxyModelHolder : KotlinEpoxyHolder() {
    val root by bind<ConstraintLayout>(R.id.root)
}