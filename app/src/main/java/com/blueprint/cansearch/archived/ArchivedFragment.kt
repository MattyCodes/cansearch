package com.blueprint.cansearch.archived

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyRecyclerView
import com.airbnb.mvrx.BaseMvRxFragment
import com.airbnb.mvrx.fragmentViewModel
import com.blueprint.cansearch.R

class ArchivedFragment : BaseMvRxFragment(), OnArchivedArticleClickedListener {

    private val archivedFragmentViewModel: ArchivedFragmentViewModel by fragmentViewModel()

    private lateinit var archivedArticleController: ArchivedArticleController

    private lateinit var rcArchivedArticle: EpoxyRecyclerView
    private lateinit var textSavedArticleNumbers: TextView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        archivedArticleController = ArchivedArticleController(this)
        setupRecyclerView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_archived, container, false)
        rcArchivedArticle = root.findViewById(R.id.rcArchivedArticle)
        textSavedArticleNumbers = root.findViewById(R.id.textSavedArticleNumbers)
        return root
    }

    override fun onResume() {
        super.onResume()
        subscribeToAllRetrievingAllTrials()
        archivedFragmentViewModel.retrieveAllTrials()
    }

    override fun invalidate() {

    }

    override fun onArticleClicked(id: String) {
        findNavController().navigate(ArchivedFragmentDirections.actionArchivedFragmentToDetailedClinicalTrialFragment(trialId = null, searchQuery = null, storedArticleId = id))
    }

    private fun subscribeToAllRetrievingAllTrials() {
        archivedFragmentViewModel.asyncSubscribe(this, ArchivedTrialsState::allClinicalTrials, onSuccess = {
            if (it.isNotEmpty()) {
                val suffix = if (it.size > 1) "Articles Stored" else "Article Stored"
                archivedArticleController.setData(it.sortedByDescending { it.timestamp })
                textSavedArticleNumbers.text = "${it.size} $suffix"
            } else {
                textSavedArticleNumbers.text = "0 Articles stored"
            }
        })
    }

    private fun setupRecyclerView() {
        rcArchivedArticle.adapter = archivedArticleController.adapter
        rcArchivedArticle.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
    }
}