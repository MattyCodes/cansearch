package com.blueprint.cansearch.archived

import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.blueprint.cansearch.R
import com.blueprint.cansearch.common.domain.ArchivedArticle
import com.blueprint.cansearch.common.ui.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.list_item_archived_article)
abstract class ArchivedEpoxyModel : EpoxyModelWithHolder<ArchivedEpoxyModelHolder>() {

    @EpoxyAttribute
    lateinit var archivedArticle: ArchivedArticle

    @EpoxyAttribute
    lateinit var onArchivedArticleClickedListener: OnArchivedArticleClickedListener

    override fun bind(holder: ArchivedEpoxyModelHolder) {
        with(holder) {
            root.findViewById<TextView>(R.id.textArchivedId).text = archivedArticle.articleId.capitalize()
            root.findViewById<TextView>(R.id.textArchivedTitle).text = archivedArticle.title
            root.setOnClickListener {
                onArchivedArticleClickedListener.onArticleClicked(archivedArticle.articleId)
            }
        }
    }

}

class ArchivedEpoxyModelHolder : KotlinEpoxyHolder() {
    val root by bind<ConstraintLayout>(R.id.root)
}