package com.blueprint.cansearch.archived

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val archivedModule = module {
    viewModel {
        ArchivedFragmentViewModel(
            initialState = ArchivedTrialsState(),
            retrieveAllTrialIdUseCase = get()
        )
    }
}