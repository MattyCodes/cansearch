package com.blueprint.cansearch.archived

import com.airbnb.mvrx.*
import com.blueprint.cansearch.common.domain.ArchivedArticle
import com.blueprint.cansearch.detailedtrial.ui.RetrieveAllTrialIdUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject

data class ArchivedTrialsState(
    val allClinicalTrials: Async<List<ArchivedArticle>> = Uninitialized
) : MvRxState

class ArchivedFragmentViewModel(
    val initialState: ArchivedTrialsState = ArchivedTrialsState(),
    private val retrieveAllTrialIdUseCase: RetrieveAllTrialIdUseCase
) : BaseMvRxViewModel<ArchivedTrialsState>(initialState = initialState, debugMode = BuildConfig.DEBUG) {

    fun retrieveAllTrials() {
        retrieveAllTrialIdUseCase
            .execute()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .execute { copy(allClinicalTrials = it) }
    }

    companion object : MvRxViewModelFactory<ArchivedFragmentViewModel, ArchivedTrialsState> {
        override fun create(viewModelContext: ViewModelContext, state: ArchivedTrialsState): ArchivedFragmentViewModel {

            val retrieveAllTrials = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<RetrieveAllTrialIdUseCase>()
            }.value

            return ArchivedFragmentViewModel(state, retrieveAllTrials)
        }
    }
}
