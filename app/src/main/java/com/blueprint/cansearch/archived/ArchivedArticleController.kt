package com.blueprint.cansearch.archived

import com.airbnb.epoxy.TypedEpoxyController
import com.blueprint.cansearch.common.domain.ArchivedArticle

class ArchivedArticleController(val onArchivedArticleClickedListener: OnArchivedArticleClickedListener) : TypedEpoxyController<List<ArchivedArticle>>() {

    override fun buildModels(data: List<ArchivedArticle>) {
        data.forEach {
            archived {
                id(it.articleId)
                onArchivedArticleClickedListener(onArchivedArticleClickedListener)
                archivedArticle(it)
            }
        }
    }
}

interface OnArchivedArticleClickedListener {
    fun onArticleClicked(id: String)
}