package com.blueprint.cansearch.core.network

import io.reactivex.Observable
import io.reactivex.Single

/** MVI Components ******************/
typealias Reducer<S, C> = (state: S, change: C) -> S
/** Label interfaces */
interface Action
interface State
interface Change
/** Clean Arch Components ******************/
/** Label interfaces */
interface UseCase
/** General Components ******************/
sealed class Outcome<out T> {
    data class Success<out T>(val data: T) : Outcome<T>()
    data class Fail<out T>(val error: Error) : Outcome<T>()
}
data class Error(
    val message: String? = null,
    val cause: Throwable? = null
)
class ErrorThrowable(
    override val message: String? = null,
    private val throwable: Throwable? = null
) : Throwable(message) {
    fun toError() = Error(message, throwable)
}
fun Error.toThrowable(): ErrorThrowable {
    return ErrorThrowable(message, cause)
}
fun <T> Error.toOutcome(): Outcome<T> {
    return Outcome.Fail(this)
}
fun <T> Throwable.toErrorOutcome(): Outcome<T> {
    return when (this) {
        is ErrorThrowable -> Outcome.Fail(this.toError())
        else -> Outcome.Fail(ErrorThrowable(message, this).toError())
    }
}
inline fun <T, A : Any> Outcome<T>.map(mapFunction: (T) -> A): Outcome<A> {
    return when (this) {
        is Outcome.Success -> Outcome.Success(mapFunction(this.data))
        is Outcome.Fail -> Outcome.Fail(this.error)
    }
}
fun <T> Outcome<T>.toDataObservable(): Observable<T> {
    return when (this) {
        is Outcome.Success -> Observable.just(this.data)
        is Outcome.Fail -> Observable.error(this.error.toThrowable())
    }
}
fun <T> Outcome<T>.toObservable(): Observable<Outcome<T>> {
    return when (this) {
        is Outcome.Success -> Observable.just(this)
        is Outcome.Fail -> Observable.error(this.error.toThrowable())
    }
}
fun <T> Outcome<T>.toDataSingle(): Single<T> {
    return when (this) {
        is Outcome.Success -> Single.just(this.data)
        is Outcome.Fail -> Single.error(this.error.toThrowable())
    }
}
fun <T> Outcome<T>.toSingle(): Single<Outcome<T>> {
    return when (this) {
        is Outcome.Success -> Single.just(this)
        is Outcome.Fail -> Single.error(this.error.toThrowable())
    }
}
fun <T> Single<Outcome<T>>.toDataSingle() = flatMap { it.toDataSingle() }
fun <T> Observable<Outcome<T>>.toDataObservable() = flatMap { it.toDataObservable() }
fun <T> T.toOutcome(): Outcome<T> {
    return Outcome.Success(this)
}
