package com.blueprint.cansearch.core

import android.content.Context
import androidx.room.Room
import com.blueprint.cansearch.common.domain.AppDatabase
import com.google.firebase.auth.FirebaseAuth
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val coreModule = module {
    single {
        Room.databaseBuilder(
            androidApplication(),
            AppDatabase::class.java, "archivedarticle"
        ).build()
    }
    single { androidContext().getSharedPreferences(androidApplication().packageName, Context.MODE_PRIVATE) }
    single { FirebaseAuth.getInstance() }
}