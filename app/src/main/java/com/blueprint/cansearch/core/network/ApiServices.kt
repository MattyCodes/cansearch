package com.blueprint.cansearch.core.network

import com.blueprint.cansearch.search.domain.ClinicalTrials
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiServices {

    @GET("clinical-trials?size=20")
    fun searchTrials(@Query("_fulltext") searchQuery: String): Single<ClinicalTrials>

    @GET("clinical-trial/{id}")
    fun getTrial(@Path("id") trialId: String): Single<ClinicalTrials.Trials>

}