package com.blueprint.cansearch.search.di

import com.blueprint.cansearch.search.SearchClinicalTrialsUseCase
import com.blueprint.cansearch.search.data.SearchClinicalTrialsRepository
import com.blueprint.cansearch.search.ui.SearchClinicalTrialsViewModel
import com.blueprint.cansearch.search.ui.SearchState
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val searchModule = module {

    single {
        SearchClinicalTrialsRepository(
            apiService = get()
        )
    }

    single {
        SearchClinicalTrialsUseCase(
            repository = get()
        )
    }

    viewModel {
        SearchClinicalTrialsViewModel(
            initialState = SearchState(),
            searchClinicalTrialsUseCase = get()
        )
    }

}