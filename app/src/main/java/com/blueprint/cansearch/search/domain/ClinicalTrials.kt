package com.blueprint.cansearch.search.domain

import com.squareup.moshi.Json

data class ClinicalTrials(
    val total: Int,
    val trials: List<Trials>
) {
    data class Trials(
        @Json(name = "nci_id")
        val nciID: String?,

        @Json(name = "nct_id")
        val nctID: String?,

        @Json(name = "outcome_measures")
        val outcomeMeasures: List<OutcomeMeasuresItem>?,

        @Json(name = "current_trial_status")
        val trialStatus: String?,

        @Json(name = "brief_title")
        val briefTitle: String?,

        @Json(name = "official_title")
        val officialTitle: String?,

        @Json(name = "brief_summary")
        val briefSummary: String?,

        @Json(name = "detail_description")
        val detailedSummary: String?,

        val phase: Phase?,

        @Json(name = "primary_purpose")
        val primaryPurpose: PrimaryPurpose?,

        val masking: Masking? = null,

        @Json(name = "principal_investigator")
        val principalInvestigator: String?,

        @Json(name = "lead_org")
        val leadOrganization: String?,

        val biomarkers: List<Biomarkers>?,

        val collaborators: List<Collaborators>?,

        val sites: List<SitesItem>,

        val diseases: List<AssociatedDisease>?,

        val eligibility: Eligibility?,

        @Json(name = "number_of_arms")
        val numberOfArms: Int?,

        val arms: List<Arms>?

    ) {

        data class AssociatedDisease(
            @Json(name = "display_name")
            val associatedDiseaseName: String? = null
        )

        data class OutcomeMeasuresItem(
            @Json(name = "timeframe")
            val timeFrame: String,

            val name: String,

            @Json(name = "description")
            val description: String?,

            @Json(name = "type_code")
            val typeCode: String
        )

        data class Biomarkers(
            @Json(name = "name")
            val name: String? = null
        )

        data class Phase(val phase: String)

        data class PrimaryPurpose(
            @Json(name = "primary_purpose_code") val phase: String
        )

        data class Masking(
            val masking: String?,

            @Json(name = "masking_allocation_code")
            val maskingCode: String?
        )

        data class Collaborators(val name: String)

        data class SitesItem(
            @Json(name = "local_site_identifier")
            val localSiteIdentifier: String? = null,

            @Json(name = "org_state_or_province")
            val orgStateOrProvince: String? = null,

            @Json(name = "contact_name")
            val contactName: String? = null,

            @Json(name = "contact_phone")
            val contactPhone: String? = null,

            @Json(name = "org_address_line_2")
            val orgAddressLine2: Any? = null,

            @Json(name = "org_address_line_1")
            val orgAddressLine1: String? = null,

            @Json(name = "org_family")
            val orgFamily: Any? = null,

            @Json(name = "org_postal_code")
            val orgPostalCode: String? = null,

            @Json(name = "contact_email")
            val contactEmail: String? = null,

            @Json(name = "recruitment_status")
            val recruitmentStatus: String? = null,

            @Json(name = "org_city")
            val orgCity: String? = null,

            @Json(name = "org_email")
            val orgEmail: Any? = null,

            @Json(name = "org_country")
            val orgCountry: String? = null,

            @Json(name = "org_fax")
            val orgFax: Any? = null,

            @Json(name = "org_phone")
            val orgPhone: String? = null,

            @Json(name = "org_name")
            val orgName: String? = null,

            @Json(name = "org_coordinates")
            val orgCoordinates: OrgCoordinates?
        ) {
            data class OrgCoordinates(
                val lon: Double,
                val lat: Double
            )
        }

        data class Eligibility(val structured: Structured) {

            data class Structured(
                @Json(name = "max_age_in_years")
                val maxAgeInYears: Int?,

                @Json(name = "gender")
                val gender: String?,

                @Json(name = "min_age_in_years")
                val minAgeInYears: Float?
            )
        }

        data class Arms(
            val interventions: List<InterventionsItem>,

            @Json(name = "arm_type")
            val armType: String?,

            @Json(name = "arm_name")
            val armName: String?,

            @Json(name = "arm_description")
            val armDescription: String?
        ) {
            data class InterventionsItem(
                @Json(name = "intervention_name")
                val interventionName: String?
            )
        }
    }
}

data class Associated(
    val diseases: List<ClinicalTrials.Trials.AssociatedDisease>?,
    val biomarkers: List<ClinicalTrials.Trials.Biomarkers>?
)
