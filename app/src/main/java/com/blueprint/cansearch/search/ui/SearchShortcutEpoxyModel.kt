package com.blueprint.cansearch.search.ui

import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.blueprint.cansearch.R
import com.blueprint.cansearch.common.ui.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.list_item_search_shortcut)
abstract class SearchShortcutEpoxyModel : EpoxyModelWithHolder<SearchShortcutEpoxyModelHolder>() {

    @EpoxyAttribute
    lateinit var searchShortCut: String

    @EpoxyAttribute
    lateinit var onSearchShortcutClickListener: OnShortcutSelectedListener

    override fun bind(holder: SearchShortcutEpoxyModelHolder) {
        with(holder) {
            rootView.findViewById<TextView>(R.id.textShortcutSearch).text = searchShortCut
            rootView.setOnClickListener {
                onSearchShortcutClickListener.onShortCutSelected(searchShortCut)
            }
        }
    }
}

class SearchShortcutEpoxyModelHolder : KotlinEpoxyHolder() {
    val rootView by bind<ConstraintLayout>(R.id.searchShortcutContainer)
}