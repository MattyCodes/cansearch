package com.blueprint.cansearch.search.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.blueprint.cansearch.R
import com.blueprint.cansearch.search.domain.ClinicalTrials

class ClinicalTrialsRecyclerViewAdapter(
    var clinicalTrialsList: List<ClinicalTrials.Trials> = emptyList()
) : RecyclerView.Adapter<ClinicalTrialsViewHolder>() {

    var onClinicalTrialClickListener: OnClinicalTrialClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClinicalTrialsViewHolder {
        return ClinicalTrialsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_clinical_trial_preview,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = clinicalTrialsList.size

    override fun onBindViewHolder(holder: ClinicalTrialsViewHolder, position: Int) {
        holder.bindItem(clinicalTrialsList[position])
        holder.containerClinicalTrial.setOnClickListener {
            onClinicalTrialClickListener?.let {
                it.onClinicalTrialClicked(clinicalTrialsList[position])
            }
        }
    }

}

class ClinicalTrialsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val textInstituteTitle: TextView = itemView.findViewById(R.id.textInstituteTitle)
    private val textPrincipleInvestigatorTitle: TextView = itemView.findViewById(R.id.textPrincipleInvestigatorTitle)
    private val textNumberOfTrials: TextView = itemView.findViewById(R.id.textNumberOfTrials)
    private val textStudyTitle: TextView = itemView.findViewById(R.id.textStudyTitle)
    val containerClinicalTrial: ConstraintLayout = itemView.findViewById(R.id.containerClinicalTrial)

    fun bindItem(trial: ClinicalTrials.Trials) {
        textInstituteTitle.text = trial.leadOrganization
        textPrincipleInvestigatorTitle.text = trial.principalInvestigator
        textNumberOfTrials.text = "${trial.sites.size} Locations"
        textStudyTitle.text = trial.briefTitle
    }
}

interface OnClinicalTrialClickListener {
    fun onClinicalTrialClicked(trial: ClinicalTrials.Trials)
}
