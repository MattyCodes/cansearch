package com.blueprint.cansearch.search.data

import com.blueprint.cansearch.core.network.ApiServices


class SearchClinicalTrialsRepository(private val apiService: ApiServices) {
    fun searchClinicalTrials(clinicalTrialsRequest: String) = apiService.searchTrials(clinicalTrialsRequest)
}