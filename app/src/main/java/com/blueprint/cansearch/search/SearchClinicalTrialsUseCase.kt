package com.blueprint.cansearch.search

import com.blueprint.cansearch.search.domain.ClinicalTrials
import com.blueprint.cansearch.search.data.SearchClinicalTrialsRepository
import io.reactivex.Single

class SearchClinicalTrialsUseCase(private val repository: SearchClinicalTrialsRepository) {
    fun execute(searchRequest: String): Single<ClinicalTrials> {
        return repository.searchClinicalTrials(searchRequest)
    }
}