package com.blueprint.cansearch.search.domain

import com.blueprint.cansearch.R
import com.blueprint.cansearch.detailedtrial.domain.EligibilityCriteria
import com.blueprint.cansearch.detailedtrial.domain.StudySummaryItem

fun ClinicalTrials.Trials.mapToStudySummary(): List<StudySummaryItem> {
    val studySummaryList = mutableListOf<StudySummaryItem>()
    this.principalInvestigator?.let {
        studySummaryList.add(StudySummaryItem("Principle Investigator", it, android.R.color.black))
    }
    this.leadOrganization?.let {
        studySummaryList.add(StudySummaryItem("Lead Organization", it, android.R.color.black))
    }
    this.phase?.let {
        studySummaryList.add(StudySummaryItem("Phase", it.phase, R.color.title_text_tertiary))
    }
    this.trialStatus?.let {
        studySummaryList.add(StudySummaryItem("Activity Status", it, R.color.title_text_tertiary))
    }
    this.primaryPurpose?.let {
        studySummaryList.add(StudySummaryItem("Primary Purpose", it.phase, R.color.title_text_tertiary))
    }
//    this.biomarkers?.let {
//        studySummaryList.add(StudySummaryItem("Anatomic Site", it.first().name, android.R.color.black))
//    }
    return studySummaryList
}

fun ClinicalTrials.Trials.mapToEligibilityCriteria(): List<EligibilityCriteria> {
    val eligibilitySummaryList = mutableListOf<EligibilityCriteria>()
    this.eligibility?.let {
        it.structured.gender?.let { gender ->
            eligibilitySummaryList.add(EligibilityCriteria("Gender", gender))
        }
        it.structured.minAgeInYears?.let { minAge ->
            eligibilitySummaryList.add(EligibilityCriteria("Min Age", "${minAge.toInt()}"))
        }
        it.structured.maxAgeInYears?.let { maxAge ->
            eligibilitySummaryList.add(
                EligibilityCriteria(
                    "Max Age",
                    if (maxAge > 100) "N/A" else "$maxAge"
                )
            )
        }
    }
    return eligibilitySummaryList
}

