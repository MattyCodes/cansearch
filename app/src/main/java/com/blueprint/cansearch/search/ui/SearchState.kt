package com.blueprint.cansearch.search.ui

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MvRxState
import com.airbnb.mvrx.Uninitialized
import com.blueprint.cansearch.search.domain.ClinicalTrials

data class SearchState (val clinicalTrialsSearchResult: Async<Pair<ClinicalTrials, String>> = Uninitialized) : MvRxState