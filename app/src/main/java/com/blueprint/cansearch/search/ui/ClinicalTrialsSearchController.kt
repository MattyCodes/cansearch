package com.blueprint.cansearch.search.ui

import com.airbnb.epoxy.Typed2EpoxyController
import com.airbnb.epoxy.Typed3EpoxyController
import com.blueprint.cansearch.search.domain.ClinicalTrials

class ClinicalTrialsSearchController(
    private val onClinicalTrialClickListener: OnClinicalTrialClickListener,
    private val onShortcutSelectedListener: OnShortcutSelectedListener
) : Typed3EpoxyController<List<ClinicalTrials.Trials>, String, List<String>>() {

    override fun buildModels(
        data: List<ClinicalTrials.Trials>,
        query: String,
        shortcutList: List<String>
    ) {
        if (data.isNotEmpty()) {
            data.forEachIndexed { index, s ->
                clinicalTrial {
                    id(index)
                    query(query)
                    trial(s)
                    onClinicalTrialClickListener(onClinicalTrialClickListener)
                }
            }
        } else {
            shortcutList.forEachIndexed { _, s ->
                searchShortcut {
                    id(s)
                    searchShortCut(s)
                    onSearchShortcutClickListener(onShortcutSelectedListener)
                }
            }
        }
    }
}