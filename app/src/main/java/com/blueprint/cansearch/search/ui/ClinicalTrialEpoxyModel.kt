package com.blueprint.cansearch.search.ui

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.blueprint.cansearch.R
import com.blueprint.cansearch.common.ui.KotlinEpoxyHolder
import com.blueprint.cansearch.search.domain.ClinicalTrials
import com.google.android.material.card.MaterialCardView

@EpoxyModelClass(layout = R.layout.list_item_clinical_trial_preview)
abstract class ClinicalTrialEpoxyModel : EpoxyModelWithHolder<ClinicalTrialEpoxyModelHolder>() {

    @EpoxyAttribute
    lateinit var trial: ClinicalTrials.Trials

    @EpoxyAttribute
    lateinit var query: String

    @EpoxyAttribute
    lateinit var onClinicalTrialClickListener: OnClinicalTrialClickListener

    override fun bind(holder: ClinicalTrialEpoxyModelHolder) {
        with(holder) {
            val queryWordList = query.split(" ")
            val spannable = SpannableString(trial.briefTitle)
            queryWordList.forEach {
                val indexOfQuery = trial.briefTitle?.toLowerCase()?.indexOf(it.toLowerCase())
                indexOfQuery?.let { index ->
                    if (index > -1) {
                        spannable.setSpan(
                            ForegroundColorSpan(root.context.resources.getColor(R.color.chip_color_biomarkers)),
                            index, index + it.length,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                    }
                }
            }
            root.findViewById<TextView>(R.id.textStudyTitle).text = spannable
            root.findViewById<TextView>(R.id.textNumberOfTrials).text = "${trial.sites.size} Locations"
            root.findViewById<TextView>(R.id.textPrincipleInvestigatorTitle).text = trial.principalInvestigator
            root.findViewById<TextView>(R.id.textInstituteTitle).text = trial.leadOrganization
            root.findViewById<ConstraintLayout>(R.id.containerClinicalTrial).setOnClickListener {
                onClinicalTrialClickListener.onClinicalTrialClicked(trial)
            }
        }
    }
}

class ClinicalTrialEpoxyModelHolder : KotlinEpoxyHolder() {
    val root by bind<MaterialCardView>(R.id.root)
}