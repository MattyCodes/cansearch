package com.blueprint.cansearch.search.ui

import android.util.Log
import com.airbnb.mvrx.*
import com.blueprint.cansearch.BuildConfig
import com.blueprint.cansearch.search.SearchClinicalTrialsUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject

class SearchClinicalTrialsViewModel(
    val initialState: SearchState = SearchState(),
    private val searchClinicalTrialsUseCase: SearchClinicalTrialsUseCase
) : BaseMvRxViewModel<SearchState>
    (initialState = initialState, debugMode = BuildConfig.DEBUG) {

    init {
        Log.i("Initialized:", "SearchClinicalTrialsViewModel")
    }

    fun searchClinicalTrials(searchQuery: String) {
        searchClinicalTrialsUseCase.execute(searchQuery)
            .doOnSubscribe {
                setState { SearchState(clinicalTrialsSearchResult = Loading()) }
            }
            .doOnError { error ->
                setState { copy(clinicalTrialsSearchResult = Fail(error)) }
            }
            .doOnSuccess { searchResult ->
                setState { copy(clinicalTrialsSearchResult = Success(Pair(searchResult, searchQuery))) }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

            },
                {
                    Log.e("Fail Network", it.localizedMessage)
                })
            .disposeOnClear()
    }

    companion object : MvRxViewModelFactory<SearchClinicalTrialsViewModel, SearchState> {
        override fun create(viewModelContext: ViewModelContext, state: SearchState): SearchClinicalTrialsViewModel {
            val loginUseCase = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<SearchClinicalTrialsUseCase>()
            }.value

            return SearchClinicalTrialsViewModel(state, loginUseCase)
        }
    }

}