package com.blueprint.cansearch.search.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.mvrx.*
import com.blueprint.cansearch.R
import com.blueprint.cansearch.search.domain.ClinicalTrials

class SearchFragment : BaseMvRxFragment(), OnShortcutSelectedListener, OnClinicalTrialClickListener {

    private val searchClinicalTrialsViewModel: SearchClinicalTrialsViewModel by activityViewModel()

    // view variables
    private lateinit var searchResultsController: ClinicalTrialsSearchController

    // views
    private lateinit var recyclerViewSearchResults: RecyclerView
    private lateinit var progressBarLoading: ProgressBar
    private lateinit var textSearchResultsTitle: TextView
    private lateinit var editTextSearchInput: EditText

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_search, container, false)
        recyclerViewSearchResults = root.findViewById(R.id.recyclerViewSearchResults)
        progressBarLoading = root.findViewById(R.id.progressBarLoading)
        textSearchResultsTitle = root.findViewById(R.id.textSearchResultsTitle)
        editTextSearchInput = root.findViewById(R.id.editTextSearchInput)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchResultsController = ClinicalTrialsSearchController(this, this)
        setupSearchResultsRecyclerView()
        setOnClickListeners()
    }

    override fun onResume() {
        super.onResume()
        subscribeToSearchResults()
    }

    override fun invalidate(): Unit = withState(searchClinicalTrialsViewModel) { state ->
        when (state.clinicalTrialsSearchResult) {
            is Uninitialized -> {
                searchResultsController.setData(emptyList(), "", resources.getStringArray(R.array.search_shortcuts).toList())
            }
            is Loading -> {
                recyclerViewSearchResults.animate().alpha(0f).setInterpolator(AccelerateInterpolator()).setDuration(200).start()
                progressBarLoading.animate().alpha(1f).setInterpolator(AccelerateInterpolator()).setDuration(200).start()
                Log.i("Search clinical trials", "Loading")
            }
            is Success -> {
                progressBarLoading.animate().alpha(0f).setInterpolator(AccelerateInterpolator()).setDuration(200).start()
                Log.i("Search clinical trials", "Success")
            }
            is Fail -> {
                textSearchResultsTitle.text = ""
                progressBarLoading.animate().alpha(0f).setInterpolator(AccelerateInterpolator()).setDuration(200).start()
                Log.i("Search clinical trials", "Fail")
            }
        }
    }

    override fun onShortCutSelected(cancerType: String) {
        searchClinicalTrialsViewModel.searchClinicalTrials(cancerType)
    }

    override fun onClinicalTrialClicked(trial: ClinicalTrials.Trials) {
        val searchQuery = withState(searchClinicalTrialsViewModel) { state -> state.clinicalTrialsSearchResult.invoke()!!.second }
        if (trial.nciID != null) {
            findNavController().navigate(SearchFragmentDirections.actionSearchFragmentToDetailedClinicalTrialFragment(trial.nciID, searchQuery, null))
        } else if (trial.nctID != null) {
            findNavController().navigate(SearchFragmentDirections.actionSearchFragmentToDetailedClinicalTrialFragment(trial.nctID, searchQuery, null))
        }
    }

    private fun subscribeToSearchResults() {
        searchClinicalTrialsViewModel.asyncSubscribe(SearchState::clinicalTrialsSearchResult, onSuccess = {
            searchResultsController.setData(it.first.trials, it.second, resources.getStringArray(R.array.search_shortcuts).toList())
            textSearchResultsTitle.text = "${it.first.total} trials found"
            recyclerViewSearchResults.elevation = 3f
            recyclerViewSearchResults.animate().alpha(1f).setInterpolator(AccelerateInterpolator()).setDuration(200).start()
        })
    }

    private fun setupSearchResultsRecyclerView() {
        recyclerViewSearchResults.adapter = searchResultsController.adapter
        recyclerViewSearchResults.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
    }

    private fun setOnClickListeners() {
        editTextSearchInput.setOnEditorActionListener { textView, i, keyEvent ->
            if (i == EditorInfo.IME_ACTION_SEARCH){
                searchClinicalTrialsViewModel.searchClinicalTrials(editTextSearchInput.text.toString())
                true
            }
            false
        }
    }

}