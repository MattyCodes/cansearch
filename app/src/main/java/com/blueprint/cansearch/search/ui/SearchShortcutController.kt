package com.blueprint.cansearch.search.ui

import com.airbnb.epoxy.TypedEpoxyController

class SearchShortcutController(private val onShortcutSelectedListener: OnShortcutSelectedListener) : TypedEpoxyController<List<String>>() {

    override fun buildModels(data: List<String>) {
        data.forEachIndexed { _, s ->
            searchShortcut {
                id(s)
                searchShortCut(s)
                onSearchShortcutClickListener(onShortcutSelectedListener)
            }
        }
    }
}

interface OnShortcutSelectedListener {
    fun onShortCutSelected(cancerType: String)
}