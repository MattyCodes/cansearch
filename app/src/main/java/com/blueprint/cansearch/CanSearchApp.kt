package com.blueprint.cansearch

import android.app.Application
import com.blueprint.cansearch.archived.archivedModule
import com.blueprint.cansearch.core.apiServiceModule
import com.blueprint.cansearch.core.coreModule
import com.blueprint.cansearch.detailedtrial.di.detailedTrialModule
import com.blueprint.cansearch.auth.di.authModule
import com.blueprint.cansearch.auth.ui.settingsModule
import com.blueprint.cansearch.locations.di.locationModule
import com.blueprint.cansearch.search.di.searchModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

open class CanSearchApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@CanSearchApp)
            modules(
                listOf(
                    coreModule,
                    apiServiceModule,
                    searchModule,
                    detailedTrialModule,
                    locationModule,
                    authModule,
                    archivedModule,
                    settingsModule
                )
            )
        }
    }
}