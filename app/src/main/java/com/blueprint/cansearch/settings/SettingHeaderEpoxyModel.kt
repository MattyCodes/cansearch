package com.blueprint.cansearch.settings

import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.blueprint.cansearch.R
import com.blueprint.cansearch.common.ui.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.list_item_settings_header)
abstract class SettingHeaderEpoxyModel : EpoxyModelWithHolder<SettingHeaderEpoxyModelHolder>() {

    @EpoxyAttribute
    lateinit var settingsHeader: SettingsOption

    override fun bind(holder: SettingHeaderEpoxyModelHolder) {
        with(holder) {
            rootView.findViewById<TextView>(R.id.textSettingsHeader).text = settingsHeader.title
        }
    }
}

class SettingHeaderEpoxyModelHolder : KotlinEpoxyHolder() {
    val rootView by bind<ConstraintLayout>(R.id.rootView)
}