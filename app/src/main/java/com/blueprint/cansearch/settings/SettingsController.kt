package com.blueprint.cansearch.settings

import com.airbnb.epoxy.TypedEpoxyController

class SettingsController(val onSettingClickedListener: OnSettingClickedListener) : TypedEpoxyController<List<SettingsOption>>() {

    override fun buildModels(data: List<SettingsOption>) {
        data.forEach {
            if (it.type == headerType){
                settingHeader {
                    id(it.title)
                    settingsHeader(it)
                }
            } else {
                settingOption {
                    id(it.title)
                    settingsOption(it)
                    onSettingClickedListener(onSettingClickedListener)
                }
            }
        }
    }
}

interface OnSettingClickedListener {
    fun onSettingClicked(type: Int)
}