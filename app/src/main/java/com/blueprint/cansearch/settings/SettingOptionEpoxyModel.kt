package com.blueprint.cansearch.settings

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.blueprint.cansearch.R
import com.blueprint.cansearch.common.ui.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.list_item_settings_option)
abstract class SettingOptionEpoxyModel : EpoxyModelWithHolder<SettingOptionEpoxyModelHolder>() {

    @EpoxyAttribute
    lateinit var settingsOption: SettingsOption

    @EpoxyAttribute
    lateinit var onSettingClickedListener: OnSettingClickedListener

    override fun bind(holder: SettingOptionEpoxyModelHolder) {
        with(holder) {

            settingsOption.drawable?.let {
                rootView.findViewById<ImageView>(R.id.ivSettingIcon).setImageResource(it)
            } ?: kotlin.run {
                rootView.findViewById<ImageView>(R.id.ivSettingIcon).visibility = View.GONE
            }
            val textTitle = rootView.findViewById<TextView>(R.id.textSettingsHeader)
            textTitle.setTextColor(ContextCompat.getColor(rootView.context, getTextColor(settingsOption.type)))
            textTitle.text = settingsOption.title
            rootView.setOnClickListener {
                onSettingClickedListener.onSettingClicked(settingsOption.type)
            }
        }
    }

    private fun getTextColor(optionType: Int) =
        when (optionType) {
            SettingsOptionConstants.logoutOption.type -> {
                R.color.logout_color
            }
            SettingsOptionConstants.deleteAccountOption.type -> {
                R.color.delete_account
            }
            else -> {
                R.color.text_dark_secondary
            }
        }

}

class SettingOptionEpoxyModelHolder : KotlinEpoxyHolder() {
    val rootView by bind<ConstraintLayout>(R.id.rootView)
}

