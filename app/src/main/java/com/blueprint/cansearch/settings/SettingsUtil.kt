package com.blueprint.cansearch.settings

import com.blueprint.cansearch.R

const val headerType = 1
const val searchFilterType = 2
const val savedArticleType = 3
const val aboutCansearchType = 4
const val developerStoryType = 5
const val contactType = 6
const val castVoteType = 7
const val changeEmailType = 8
const val changePasswordType = 9
const val logOutType = 10
const val deleteAccountType = 11

data class SettingsOption(val title: String, val type: Int, val drawable: Int? = null) {
    companion object {
        val settingsList = listOf(
            SettingsOptionConstants.searchHeader,
            SettingsOptionConstants.searchFiltersOption,
            SettingsOptionConstants.archivedArticleOption,
            SettingsOptionConstants.cansearchHeader,
            SettingsOptionConstants.aboutOption,
            SettingsOptionConstants.developerOption,
            SettingsOptionConstants.contactOption,
            SettingsOptionConstants.donationHeader,
            SettingsOptionConstants.voteOption,
            SettingsOptionConstants.profileHeader,
            SettingsOptionConstants.changeEmailOption,
            SettingsOptionConstants.changePasswordOption,
            SettingsOptionConstants.logoutOption,
            SettingsOptionConstants.deleteAccountOption
        )
    }
}

object SettingsOptionConstants {
    val profileHeader = SettingsOption("Profile", headerType)
    val searchHeader = SettingsOption("Search", headerType)
    val donationHeader = SettingsOption("Donation", headerType)
    val cansearchHeader = SettingsOption("Cansearch", headerType)

    val searchFiltersOption = SettingsOption("Search Filters", searchFilterType, R.drawable.ic_filter)
    val archivedArticleOption = SettingsOption("Clear Saved Articles", savedArticleType, R.drawable.ic_archive)
    val aboutOption = SettingsOption("About Cansearch", aboutCansearchType, R.drawable.ic_about)
    val developerOption = SettingsOption("Developer Story", developerStoryType, R.drawable.ic_dev_story)
    val contactOption = SettingsOption("Contact Cansearch", contactType, R.drawable.ic_email)
    val voteOption = SettingsOption("Cast Your Vote", castVoteType, R.drawable.ic_vote_ph)
    val changeEmailOption = SettingsOption("Change Email", changeEmailType)
    val changePasswordOption = SettingsOption("Change Password", changePasswordType)
    val logoutOption = SettingsOption("Log Out", logOutType)
    val deleteAccountOption = SettingsOption("Delete account", deleteAccountType)
}