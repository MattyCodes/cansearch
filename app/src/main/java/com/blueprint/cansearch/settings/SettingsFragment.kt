package com.blueprint.cansearch.settings

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyRecyclerView
import com.airbnb.mvrx.BaseMvRxFragment
import com.airbnb.mvrx.fragmentViewModel
import com.blueprint.cansearch.R
import com.blueprint.cansearch.auth.domain.isPasswordValid
import com.blueprint.cansearch.auth.domain.isValidEmail
import com.blueprint.cansearch.auth.ui.AuthActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import android.net.Uri

const val cansearchEmailAddress = "Cansearchapp@gmail.com"
const val userFeedback = "User Feedback"
const val deleteArchivedArticleSuccess = "Saved articles deleted successfully"
const val deleteArchivedArticleFail = "Saved articles did not deletion failed, please try again"
const val emailUpdateSuccess = "Email updated successfully"
const val emailUpdateFail = "Email failed, please try again"
const val passwordUpdateSuccess = "Password updated successfully"
const val passwordUpdateFail = "Password failed, please try again"
const val loadingAlpha = 0.2f

class SettingsFragment : BaseMvRxFragment(), OnSettingClickedListener {

    private val settingsViewModel: SettingsViewModel by fragmentViewModel()

    // view variables
    private lateinit var settingController: SettingsController
    private lateinit var rcSettings: EpoxyRecyclerView

    private lateinit var changePasswordDialog: AlertDialog
    private lateinit var changeEmailDialog: AlertDialog
    private lateinit var deleteAccountDialog: Dialog
    private lateinit var deleteSavedArticlesDialog: Dialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_settings, container, false)
        rcSettings = root.findViewById(R.id.rcSettings)
        return root
    }

    override fun onResume() {
        super.onResume()
        setupRecyclerView()
        subscribeToPasswordUpdate()
        subscribeToEmailUpdate()
        subscribeToDeleteAccount()
        subscribeToDeleteLocalDataBase()
    }

    override fun invalidate() {

    }

    override fun onSettingClicked(type: Int) {
        when (type) {
            logOutType -> {
                showLogOutDialog()
            }
            changePasswordType -> {
                showChangePasswordDialog()
            }
            changeEmailType -> {
                showChangeEmailDialog()
            }
            deleteAccountType -> {
                showDeleteAccountDialog()
            }
            savedArticleType -> {
                showDeleteLocalDatabaseDialog()
            }
            contactType -> {
                attemptEmailLaunch()
            }
        }

    }

    private fun attemptEmailLaunch() {
        val emailIntent = Intent(Intent.ACTION_SENDTO)

        val uriText = "mailto:" + Uri.encode(cansearchEmailAddress) +
                "?subject=" + Uri.encode(userFeedback)
        val uri = Uri.parse(uriText)

        emailIntent.setData(uri)
        requireContext().startActivity(Intent.createChooser(emailIntent, "Send mail..."))
    }

    private fun subscribeToDeleteLocalDataBase() {
        settingsViewModel.asyncSubscribe(SettingsState::deleteArchivedArticles, onSuccess = {
            deleteSavedArticlesDialog.dismiss()
            updateSnackBar(deleteArchivedArticleSuccess)
        }, onFail = {
            deleteSavedArticlesDialog.dismiss()
            updateSnackBar(deleteArchivedArticleFail)
        })
    }

    private fun subscribeToEmailUpdate() {
        settingsViewModel.asyncSubscribe(SettingsState::updateEmail, onSuccess = {
            changeEmailDialog.dismiss()
            updateSnackBar(emailUpdateSuccess)
        }, onFail = {
            changeEmailDialog.dismiss()
            updateSnackBar(emailUpdateFail)
        })
    }

    private fun subscribeToPasswordUpdate() {
        settingsViewModel.asyncSubscribe(SettingsState::updatePassword, onSuccess = {
            changePasswordDialog.dismiss()
            updateSnackBar(passwordUpdateSuccess)
        }, onFail = {
            changePasswordDialog.dismiss()
            updateSnackBar(passwordUpdateFail)
        })
    }

    private fun subscribeToDeleteAccount() {
        settingsViewModel.asyncSubscribe(SettingsState::deleteAccount, onSuccess = {
            deleteAccountDialog.dismiss()
            startActivity(Intent(requireContext(), AuthActivity::class.java))
            activity?.let { it.finish() }
        }, onFail = {
            deleteAccountDialog.dismiss()
            startActivity(Intent(requireContext(), AuthActivity::class.java))
            activity?.let { it.finish() }
        })
    }

    private fun showLogOutDialog() {
        val builder = AlertDialog.Builder(requireContext())
        val customLayout = layoutInflater.inflate(R.layout.dialog_logout, null)
        builder.setView(customLayout)
        val dialog = builder.create()

        val logoutConfirm: TextView = customLayout.findViewById(R.id.textConfirmCTA)
        val cancel: TextView = customLayout.findViewById(R.id.textCancel)

        cancel.setOnClickListener {
            dialog.dismiss()
        }

        logoutConfirm.setOnClickListener {
            if (dialog.isShowing) {
                FirebaseAuth.getInstance().signOut()
                dialog.dismiss()
                startActivity(Intent(requireContext(), AuthActivity::class.java))
                activity?.let { it.finish() }
            }
        }
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setOnDismissListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun showChangePasswordDialog() {
        val builder = AlertDialog.Builder(requireContext())
        val customLayout = layoutInflater.inflate(R.layout.dialog_change_password, null)
        builder.setView(customLayout)
        changePasswordDialog = builder.create()

        val changePasswordConfirm: TextView = customLayout.findViewById(R.id.textConfirmCTA)
        val cancel: TextView = customLayout.findViewById(R.id.textCancel)
        val progressBar: ProgressBar = customLayout.findViewById(R.id.progressBar)
        val textPasswordErrorPrompt: TextView = customLayout.findViewById(R.id.textPasswordErrorPrompt)
        val textDialogTitle: TextView = customLayout.findViewById(R.id.textDialogTitle)
        val passwordInput: EditText = customLayout.findViewById(R.id.editTextChangePassword)

        cancel.setOnClickListener {
            changePasswordDialog.dismiss()
        }

        changePasswordConfirm.setOnClickListener {
            if (changePasswordDialog.isShowing) {
                val newPassword = passwordInput.text.toString()
                if (newPassword.isPasswordValid()) {
                    textDialogTitle.alpha = loadingAlpha
                    passwordInput.alpha = loadingAlpha
                    cancel.alpha = loadingAlpha
                    changePasswordConfirm.alpha = loadingAlpha
                    textPasswordErrorPrompt.alpha = loadingAlpha
                    progressBar.visibility = View.VISIBLE
                    settingsViewModel.updatePassword(newPassword)
                } else {
                    textPasswordErrorPrompt.visibility = View.VISIBLE
                }
            }
        }
        changePasswordDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        changePasswordDialog.setOnDismissListener {
            changePasswordDialog.dismiss()
        }
        changePasswordDialog.show()
    }

    private fun showChangeEmailDialog() {
        val builder = AlertDialog.Builder(requireContext())
        val customLayout = layoutInflater.inflate(R.layout.dialog_change_email, null)
        builder.setView(customLayout)
        changeEmailDialog = builder.create()

        val changeEmailConfirm: TextView = customLayout.findViewById(R.id.textConfirmCTA)
        val cancel: TextView = customLayout.findViewById(R.id.textCancel)
        val progressBar: ProgressBar = customLayout.findViewById(R.id.progressBar)
        val textEmailErrorPrompt: TextView = customLayout.findViewById(R.id.textEmailErrorPrompt)
        val textDialogTitle: TextView = customLayout.findViewById(R.id.textDialogTitle)
        val editTextChangeEmail: EditText = customLayout.findViewById(R.id.editTextChangeEmail)

        cancel.setOnClickListener {
            changeEmailDialog.dismiss()
        }

        changeEmailConfirm.setOnClickListener {
            if (changeEmailDialog.isShowing) {
                val email = editTextChangeEmail.text.toString()
                if (email.isValidEmail()) {
                    textDialogTitle.alpha = loadingAlpha
                    changeEmailConfirm.alpha = loadingAlpha
                    cancel.alpha = loadingAlpha
                    textEmailErrorPrompt.alpha = loadingAlpha
                    textDialogTitle.alpha = loadingAlpha
                    progressBar.visibility = View.VISIBLE
                    settingsViewModel.updateEmail(email)
                } else {
                    textEmailErrorPrompt.visibility = View.VISIBLE
                }
            }
        }
        changeEmailDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        changeEmailDialog.setOnDismissListener {
            changeEmailDialog.dismiss()
        }
        changeEmailDialog.show()
    }

    private fun showDeleteAccountDialog() {
        val builder = AlertDialog.Builder(requireContext())
        val customLayout = layoutInflater.inflate(R.layout.dialog_delete_account, null)
        builder.setView(customLayout)
        deleteAccountDialog = builder.create()

        val deleteCTA: TextView = customLayout.findViewById(R.id.textConfirmCTA)
        val cancel: TextView = customLayout.findViewById(R.id.textCancel)
        val progressBar: ProgressBar = customLayout.findViewById(R.id.progressBar)
        val textDeleteAccountDescription: TextView = customLayout.findViewById(R.id.textDeleteAccountDescription)
        val textDialogTitle: TextView = customLayout.findViewById(R.id.textDialogTitle)

        cancel.setOnClickListener {
            deleteAccountDialog.dismiss()
        }

        deleteCTA.setOnClickListener {
            if (deleteAccountDialog.isShowing) {
                deleteCTA.alpha = loadingAlpha
                cancel.alpha = loadingAlpha
                textDeleteAccountDescription.alpha = loadingAlpha
                textDialogTitle.alpha = loadingAlpha
                progressBar.visibility = View.VISIBLE
                settingsViewModel.deleteAccount()
            }
        }
        deleteAccountDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        deleteAccountDialog.setOnDismissListener {
            deleteAccountDialog.dismiss()
        }
        deleteAccountDialog.show()
    }

    private fun showDeleteLocalDatabaseDialog() {
        val builder = AlertDialog.Builder(requireContext())
        val customLayout = layoutInflater.inflate(R.layout.dialog_delete_account, null)
        builder.setView(customLayout)
        deleteSavedArticlesDialog = builder.create()

        val deleteCTA: TextView = customLayout.findViewById(R.id.textConfirmCTA)
        val cancel: TextView = customLayout.findViewById(R.id.textCancel)
        val progressBar: ProgressBar = customLayout.findViewById(R.id.progressBar)
        val textDeleteAccountDescription: TextView = customLayout.findViewById(R.id.textDeleteAccountDescription)
        val textDialogTitle: TextView = customLayout.findViewById(R.id.textDialogTitle)
        textDialogTitle.text = "Delete Stored Clinical Trials"
        textDeleteAccountDescription.text = "Are you sure you want to delete all saved Clinical Trials. Once completed, this process cannot be reversed!"

        cancel.setOnClickListener {
            deleteSavedArticlesDialog.dismiss()
        }

        deleteCTA.setOnClickListener {
            if (deleteSavedArticlesDialog.isShowing) {
                deleteCTA.alpha = loadingAlpha
                cancel.alpha = loadingAlpha
                textDeleteAccountDescription.alpha = loadingAlpha
                textDialogTitle.alpha = loadingAlpha
                progressBar.visibility = View.VISIBLE
                settingsViewModel.deleteLocalDataBase()
            }
        }
        deleteSavedArticlesDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        deleteSavedArticlesDialog.setOnDismissListener {
            deleteSavedArticlesDialog.dismiss()
        }
        deleteSavedArticlesDialog.show()
    }

    private fun setupRecyclerView() {
        settingController = SettingsController(this)
        rcSettings.adapter = settingController.adapter
        rcSettings.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        settingController.setData(SettingsOption.settingsList)
    }

    private fun updateSnackBar(snackBarMessage: String) {
        val snackBar = Snackbar.make(requireView(), snackBarMessage, Snackbar.LENGTH_LONG)
        snackBar.view.background = resources.getDrawable(R.drawable.background_main_gradient, null)
        snackBar.show()
    }
}