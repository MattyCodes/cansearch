package com.blueprint.cansearch.settings

import com.airbnb.mvrx.*
import com.blueprint.cansearch.auth.ui.DeleteAccountUseCase
import com.blueprint.cansearch.auth.ui.UpdateUserEmailUseCase
import com.blueprint.cansearch.auth.ui.UpdateUserPasswordUseCase
import org.koin.android.ext.android.inject
import org.koin.core.KoinComponent

data class SettingsState(
    val updateEmail: Async<Boolean> = Uninitialized,
    val updatePassword: Async<Boolean> = Uninitialized,
    val deleteAccount: Async<Any> = Uninitialized,
    val deleteArchivedArticles: Async<Any> = Uninitialized
) : MvRxState

class SettingsViewModel(
    initialState: SettingsState = SettingsState(),
    private val updateUserEmailUseCase: UpdateUserEmailUseCase,
    private val updateUserPasswordUseCase: UpdateUserPasswordUseCase,
    private val deleteAccountUseCase: DeleteAccountUseCase
) : BaseMvRxViewModel<SettingsState>(initialState, debugMode = BuildConfig.DEBUG), KoinComponent {

    init {
        asyncSubscribe(SettingsState::updateEmail, onSuccess = {
            setState { copy(updateEmail = Uninitialized) }
        }, onFail = {
            setState { copy(updateEmail = Uninitialized) }
        })

        asyncSubscribe(SettingsState::updatePassword, onSuccess = {
            setState { copy(updatePassword = Uninitialized) }
        }, onFail = {
            setState { copy(updatePassword = Uninitialized) }
        })

        asyncSubscribe(SettingsState::deleteArchivedArticles, onSuccess = {
            setState { copy(deleteArchivedArticles = Uninitialized) }
        }, onFail = {
            setState { copy(deleteArchivedArticles = Uninitialized) }
        })
    }

    fun updateEmail(email: String) {
        updateUserEmailUseCase.create(email).execute { copy(updateEmail = it) }
    }

    fun updatePassword(password: String) {
        updateUserPasswordUseCase.create(password).execute { copy(updatePassword = it) }
    }

    fun deleteAccount() {
        deleteAccountUseCase.create().execute { copy(deleteAccount = it) }
    }

    fun deleteLocalDataBase() {
        deleteAccountUseCase.deleteLocalDataBase().execute { copy(deleteArchivedArticles = it) }
    }

    companion object : MvRxViewModelFactory<SettingsViewModel, SettingsState> {
        override fun create(viewModelContext: ViewModelContext, state: SettingsState): SettingsViewModel {

            val updateEmail = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<UpdateUserEmailUseCase>()
            }.value

            val updatePassword = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<UpdateUserPasswordUseCase>()
            }.value

            val deleteAccount = when (viewModelContext) {
                is FragmentViewModelContext -> viewModelContext.fragment.inject()
                is ActivityViewModelContext -> viewModelContext.activity.inject<DeleteAccountUseCase>()
            }.value

            return SettingsViewModel(
                state,
                updateEmail,
                updatePassword,
                deleteAccount
            )
        }
    }

}